from django.contrib import admin
from django.contrib.admin import AdminSite
from django.urls import path
from . import views

# for admin views
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

from .models import Importer, Exporter, ImportLog

class CustomAdminSite(AdminSite):
    site_header = 'Vandelay Administration'
    def get_urls(self):
        urls = super(CustomAdminSite, self).get_urls()
        custom_urls = [
            path('import/', self.admin_view(self.import_dashboard), name='import_dashboard'),
            path('import/videos', self.admin_view(self.import_videos), name='import_videos'),
            path('import/posts', self.admin_view(self.import_posts), name='import_posts'),
            path('import/artists', self.admin_view(self.import_artists), name='import_artists'),
            path('import/artists-from-spreadsheet', self.admin_view(self.import_artists_from_spreadsheet), name='import_artists_from_spreadsheet'),
            path('import/connect-artist-work-images', self.admin_view(self.connect_artist_work_images), name='connect_artist_work_images'),
            path('import/connect-artist-install-images', self.admin_view(self.connect_artist_install_images), name='connect_artist_install_images'),
            path('import/exhibitions', self.admin_view(self.import_exhibitions), name='import_exhibitions'),
            path('import/connect-exhibition-images', self.admin_view(self.connect_exhibition_images), name='connect_exhibition_images'),
            path('import/artworks', self.admin_view(self.import_artworks), name='import_artworks'),
            path('import/artwork-mediums', self.admin_view(self.import_artwork_mediums), name='import_artwork_mediums'),
            path('import/artwork-aspects', self.admin_view(self.import_artwork_aspects), name='import_artwork_aspects'),
            path('import/artwork-types', self.admin_view(self.import_artwork_types), name='import_artwork_types'),
            path('import/artwork-locations', self.admin_view(self.import_artwork_locations), name='import_artwork_locations'),
            path('import/artworks-from-spreadsheet', self.admin_view(self.import_artworks_from_spreadsheet), name='import_artworks_from_spreadsheet'),
            path('import/connect-artwork-mediums', self.admin_view(self.connect_artwork_mediums), name='connect_artwork_mediums'),
            path('import/connect-artwork-aspects', self.admin_view(self.connect_artwork_aspects), name='connect_artwork_aspects'),
            path('import/connect-artwork-artists', self.admin_view(self.connect_artwork_artists), name='connect_artwork_artists'),
            path('import/images', self.admin_view(self.import_images), name='import_images'),
            path('import/connect-artwork-images', self.admin_view(self.connect_artwork_images), name='connect_artwork_images'),
            path('import/contacts', self.admin_view(self.import_contacts), name='import_contacts'),
            path('import/contact-addresses', self.admin_view(self.import_contact_addresses), name='import_contact_addresses'),
            path('import/contact-phones', self.admin_view(self.import_contact_phones), name='import_contact_phones'),
            path('import/contact-categories', self.admin_view(self.import_contact_categories), name='import_contact_categories'),
            path('import/contact-emails', self.admin_view(self.import_contact_emails), name='import_contact_emails'),
            path('import/connect-contact-artists', self.admin_view(self.connect_contact_artists), name='connect_contact_artists'),
            path('import/connect-contact-categories', self.admin_view(self.connect_contact_categories), name='connect_contact_categories'),
            path('import/consignments', self.admin_view(self.import_consignments), name='import_consignments'),
            path('import/invoices', self.admin_view(self.import_invoices), name='import_invoices'),
            path('export/', self.admin_view(self.export_dashboard), name='export_dashboard'),
            path('export/ar', self.admin_view(self.export_ar), name='export_ar'),
        ]
        return urls + custom_urls

    def import_dashboard(self,request):
        request.current_app = self.name
        context = {}
        processes = [
            'import_videos',
            'import_posts',
            'import_artists_from_spreadsheet',
            'import_artists',
            'connect_artist_work_images',
            'connect_artist_install_images',
            'import_exhibitions',
            'connect_exhibition_images',
            'import_artworks',#
            'import_artwork_mediums',
            'import_artwork_aspects',
            'import_artwork_types',
            'import_artwork_locations',
            'import_artwork_from_spreadsheet',
            'connect_artwork_mediums',
            'connect_artwork_aspects',
            'connect_artwork_artists',
            'import_images',
            'connect_artwork_images',
            'import_contacts',
            'import_contact_categories',
            'import_contact_addresses',
            'import_contact_phones',
            'import_contact_emails',
            'connect_contact_artists',
            'connect_contact_categories',
            'import_consignments',
            'import_invoices',
        ]
        for process in processes:
            results = ImportLog.objects.filter( process_name=process ).order_by('-last_run')[:1]
            context['last_'+process] = results[0].last_run if results else "Never"
        return render(request, 'vandelay/import/index.html',context)

    def import_videos(self,request):
        Importer.import_videos()
        return HttpResponseRedirect("./")

    def import_posts(self,request):
        Importer.import_posts()
        return HttpResponseRedirect("./")

    def import_artists(self,request):
        Importer.import_artists()
        return HttpResponseRedirect("./")

    def connect_artist_work_images(self,request):
        Importer.connect_artists_to_images( key='ww_artist_images' )
        return HttpResponseRedirect("./")

    def connect_artist_install_images(self,request):
        Importer.connect_artists_to_images( key='ww_artist_installation_images' )
        return HttpResponseRedirect("./")

    def import_artists_from_spreadsheet(self,request):
        Importer.import_artists_from_spreadsheet()
        return HttpResponseRedirect("./")

    def import_exhibitions(self,request):
        Importer.import_exhibitions()
        return HttpResponseRedirect("./")

    def connect_exhibition_images(self,request):
        Importer.connect_exhibitions_to_images()
        return HttpResponseRedirect("./")

    def import_artworks(self,request):
        Importer.import_artworks()
        return HttpResponseRedirect("./")

    def import_artwork_mediums(self,request,):
        Importer.import_wp_taxonomy('medium')
        return HttpResponseRedirect("./")

    def import_artwork_aspects(self,request,):
        Importer.import_wp_taxonomy('aspect')
        return HttpResponseRedirect("./")

    def import_artwork_types(self,request,):
        Importer.import_artwork_types_from_spreadsheet()
        return HttpResponseRedirect("./")

    def import_artwork_locations(self,request,):
        Importer.import_artwork_locations_from_spreadsheet()
        return HttpResponseRedirect("./")

    def import_artworks_from_spreadsheet(self,request,):
        Importer.add_update_artworks_from_spreadsheet()
        return HttpResponseRedirect("./")

    def connect_artwork_mediums(self,request,):
        Importer.connect_artworks_to_wp_tax('medium')
        return HttpResponseRedirect("./")

    def connect_artwork_aspects(self,request,):
        Importer.connect_artworks_to_wp_tax('aspect')
        return HttpResponseRedirect("./")

    def connect_artwork_artists(self,request,):
        Importer.get_artwork_artists_from_artwork_titles()
        return HttpResponseRedirect("./")

    def import_images(self,request,):
        Importer.import_images()
        return HttpResponseRedirect("./")

    def connect_artwork_images(self,request,):
        Importer.connect_artworks_to_images()
        return HttpResponseRedirect("./")

    def import_contacts(self,request):
        Importer.import_update_contact_basics_from_spreadsheet()
        return HttpResponseRedirect("./")

    def import_contact_addresses(self,request):
        Importer.import_update_addresses_from_spreadsheet()
        return HttpResponseRedirect("./")

    def import_contact_phones(self,request):
        Importer.import_telephones_from_spreadsheet()
        return HttpResponseRedirect("./")

    def import_contact_categories(self,request,):
        Importer.create_update_contact_categories_from_spreadsheet()
        return HttpResponseRedirect("./")

    def import_contact_emails(self,request):
        Importer.import_emails_from_spreadsheet()
        return HttpResponseRedirect("./")

    def connect_contact_artists(self,request):
        Importer.import_contact_artist_preferences_from_spreadsheet()
        return HttpResponseRedirect("./")

    def connect_contact_categories(self,request):
        Importer.import_contact_categories_from_spreadsheet()
        return HttpResponseRedirect("./")

    def import_consignments(self,request):
        Importer.add_update_consignments()
        return HttpResponseRedirect("./")

    def import_invoices(self,request):
        Importer.add_update_invoices()
        return HttpResponseRedirect("./")

    def export_dashboard(self,request):
        request.current_app = self.name
        return render(request, 'vandelay/export/index.html')

    def export_ar(self,request,):
        data = Exporter.export_image_ar()
        return data
        # return HttpResponseRedirect("./")

vandelay_admin = CustomAdminSite(name="vandelay")

'''
def import_exhibitions( modeladmin, request, queryset ):
    Exhibition.import_exhibitions()
    return

def import_exhibitions_from_wp( modeladmin, request, queryset ):
    Exhibition.import_exhibitions_json()
    return

def import_artists( modeladmin, request, queryset ):
    Artist.import_artists()
    return

def import_artists_json( modeladmin, request, queryset ):
    Artist.import_artists_json()
    return

class PostAdmin(admin.ModelAdmin):
    list_display = ('id','title','slug','created_at','updated_at',)

class ExhibitionAdmin(admin.ModelAdmin):
    list_display = ('id','title','legacy_id','slug','post_status','created_at','updated_at',)
    list_display_links = ('id','title',)
    actions = [
        import_exhibitions,
        import_exhibitions_from_wp
    ]

class ArtistAdmin(admin.ModelAdmin):
    list_display = ('id','title','legacy_id','slug','created_at','updated_at',)
    list_display_links = ('id','title',)
    actions = [
        import_artists,
        import_artists_json
    ]
'''
# admin.site.register(Post,PostAdmin)
# admin.site.register(Exhibition,ExhibitionAdmin)
# admin.site.register(Artist,ArtistAdmin)