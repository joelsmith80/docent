import datetime
import os
import json
import csv

from django.test import TestCase
from django.utils import timezone
from dateutil import parser
from lxml.html.clean import clean_html

from project.vandelay.models import Importer
from project.docent.models import Video, Post, Exhibition, Artwork, ArtworkMedium, ArtworkAspect, ArtworkType, ArtworkImage, Image, Artist

# run this from the root folder with python manage.py test project/vandelay
# more targeted? python manage.py test project.vandelay.tests.TestImport

api_root = 'https://seattle.winstonwachter.com/wp-json/wp/v2/'

class ArtworkMediumImport(TestCase):

    def test_mocky(self):
        data = Importer.query_mocky()
        print(data)

    # test whether we can get the data from the api
    def test_query_mediums_api(self):
        data = Importer.query_api( 'mediums', api_root + 'ww-tax/mediums' )
        self.assertTrue(data)

    # test paginated query of all resources
    def test_query_mediums_api_all(self):
        data = Importer.query_taxonomy_api_all( api_root + 'ww-tax/mediums' )
        self.assertTrue( data )

    # test the formatting/prep of a single api object
    def setUp(self):

        data = Importer.query_api( 'mediums', api_root + 'ww-tax/mediums' )
        if not data: 
            raise Exception("Couldn't get data")

        entry = data[0]
        self.api_record = entry

        created_record = Importer.add_update_wp_tax_object( entry, ArtworkMedium )
        if not created_record: 
            raise Exception("Couldn't create the record")
        self.db_record = created_record

    def test_created_record_legacy_id(self):
        self.assertEqual( self.db_record.legacy_id, self.api_record['id'] )

    def test_created_record_title(self):
        self.assertEqual( self.db_record.title, self.api_record['name'].strip() )

    def test_created_record_slug(self):
        self.assertEqual( self.db_record.slug, self.api_record['slug'].strip() )

class ArtworkAspectImport(TestCase):

    # test whether we can get the data from the api
    def test_query_aspects_api(self):
        data = Importer.query_api( 'aspects', api_root + 'ww-tax/aspects' )
        self.assertTrue(data)

    # test paginated query of all resources
    def test_query_mediums_api_all(self):
        data = Importer.query_taxonomy_api_all( api_root + 'ww-tax/aspects' )
        self.assertTrue( data )

    # test the formatting/prep of a single api object
    def setUp(self):

        data = Importer.query_api( 'aspects', api_root + 'ww-tax/aspects' )
        if not data: 
            raise Exception("Couldn't get data")

        entry = data[0]
        self.api_record = entry

        created_record = Importer.add_update_wp_tax_object( entry, ArtworkAspect )
        if not created_record: 
            raise Exception("Couldn't create the record")
        self.db_record = created_record

    def test_created_record_legacy_id(self):
        self.assertEqual( self.db_record.legacy_id, self.api_record['id'] )

    def test_created_record_title(self):
        self.assertEqual( self.db_record.title, self.api_record['name'].strip() )

    def test_created_record_slug(self):
        self.assertEqual( self.db_record.slug, self.api_record['slug'].strip() )

class ArtworkTypeImport(TestCase):

    def setUp(self):

        self.doc_key = {
            'id': {
                'column_index': 0,
                'column_name': 'id'
            },
            'name': {
                'column_index': 1,
                'column_name': 'name'
            }
        }

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'inventory-types', listify = True )

        # grab the header row and some test rows
        self.header_row = self.data.pop(0)

        # now run the bulk import
        self.all_records = Importer.import_artwork_types_from_spreadsheet(self.data)

    # test that the document columns match our key
    def test_source_integrity(self):
        for key, value in self.doc_key.items():
            self.assertEqual( value['column_name'], self.header_row[value['column_index']] )

    def test_total_num_records_imported(self):
        self.assertEqual( 7, self.all_records )

class ArtworkLocationImport(TestCase):

    def setUp(self):

        self.doc_key = {
            'id': {
                'column_index': 0,
                'column_name': 'id'
            },
            'name': {
                'column_index': 1,
                'column_name': 'title'
            }
        }

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'inventory-locations', listify = True )

        # grab the header row and some test rows
        self.header_row = self.data.pop(0)

        # now run the bulk import
        self.all_records = Importer.import_artwork_locations_from_spreadsheet(self.data)

    # test that the document columns match our key
    def test_source_integrity(self):
        for key, value in self.doc_key.items():
            self.assertEqual( value['column_name'], self.header_row[value['column_index']] )

    def test_total_num_records_imported(self):
        self.assertEqual( 21, self.all_records )

class ArtworkImport(TestCase):

    # test whether we can open query the api
    def test_query_api(self):
        data = Importer.query_api('artworks','https://seattle.winstonwachter.com/wp-json/ww-scour/v2/artwork')
        self.assertTrue(data)

    # test paginated query of all resources
    def test_query_artworks_all(self):
        data = Importer.query_artworks_all()
        self.assertTrue(data)

    # create dummy records
    def setUp(self):

        data = Importer.query_api('artworks','https://seattle.winstonwachter.com/wp-json/ww-scour/v2/artwork')
        if not data: 
            raise Exception("Couldn't get data")

        entry = data[1]
        self.api_record = entry

        created_record = Importer.add_update_artwork(entry)
        if not created_record: 
            raise Exception("Couldn't create the record")
        self.db_record = created_record

    def test_created_record_id(self):
        self.assertEqual( self.db_record.legacy_id, int(self.api_record['image_id']) )

    def test_created_record_title(self):
        self.assertEqual( self.db_record.title, self.api_record['image_title'].strip() )

    def test_created_record_slug(self):
        self.assertEqual( self.db_record.slug, self.api_record['image_slug'].strip() )

    def test_created_record_post_status(self):    
        self.assertEqual( self.db_record.post_status, 'published' )

    def test_created_record_post_status(self):    
        self.assertEqual( self.db_record.post_status, 'published' )

    def test_created_record_post_status(self):    
        self.assertEqual( self.db_record.post_status, 'published' )

    def test_created_record_filemaker_id(self):  
        try:
            fm_id = self.api_record['filemaker_id']
            if not fm_id:
                fm_id = ''
        except: 
            fm_id = '' 
        self.assertEqual( self.db_record.fm_id, fm_id )

    def test_created_variations(self):
        try: api_variations = self.api_record['variations']
        except: api_variations = None
        if api_variations:
            api_variation_list = []
            for v in api_variations:
                api_variation_list.append( int(v['id']) )
            api_variation_list.sort()
            print(api_variation_list)
            record_variation_list = []
            if self.db_record.variations.all().exists():
                for v in self.db_record.variations.all():
                    record_variation_list.append( int(v.legacy_id) )
                record_variation_list.sort()
                print(record_variation_list)
            self.assertEqual( record_variation_list, api_variation_list )
        else:
            record_variations = self.db_record.variations.all().exists()
            self.assertFalse( record_variations )

class ConnectArtworkImages(TestCase):

    # create dummy records
    def setUp(self):

        # query the artworks api
        data = Importer.query_api('artworks','https://seattle.winstonwachter.com/wp-json/ww-scour/v2/artwork')
        if not data: 
            raise Exception("Couldn't get data")

        # save the first result we got
        entry = data[0]
        self.api_record = entry

        # create the record
        created_record = Importer.add_update_artwork(entry)
        if not created_record: 
            raise Exception("Couldn't create the record")
        self.db_record = created_record

        # try to get a match in the media api
        attachment_data = Importer.query_api('media','https://seattle.winstonwachter.com/wp-json/wp/v2/media/' + str(self.db_record.legacy_id))
        if not attachment_data:
            raise Exception("Couldn't find a match for attachment with legacy_id " + str(self.db_record.legacy_id) + " in media api")

        self.updated_record = Importer.connect_artwork_to_images( self.db_record, attachment_data )
        self.attachment_data = attachment_data

    def test_image_setup_worked(self):
        this_record_image_ids = []
        if self.updated_record.work_images.all():
            for t in self.updated_record.work_images.all():
                this_record_image_ids.append(t.legacy_id)
        try: attachment_image = self.attachment_data['id']
        except: attachment_image = None
        if attachment_image: attachment_image_id = [attachment_image]
        else: attachment_image_id = []
        self.assertEqual( this_record_image_ids, attachment_image_id )

class TestImageScaling(TestCase):

    def setUp(self):
        self.over_limit_data = Importer.get_scaled_image_dimensions( max_width=2000, width=3000, height=1800 )
        self.under_limit_data = Importer.get_scaled_image_dimensions( max_width=2000, width=1000, height=600 )

    def test_over_limit(self):
        self.assertEqual( (2000,1200), self.over_limit_data )

    def test_under_limit(self):
        self.assertEqual( (1000,600), self.under_limit_data )

class ConnectArtworkFeaturedImages(TestCase):

    def setUp(self):

        # create a dummy artwork
        self.artwork = Artwork(
            legacy_id = 6350,
            title = "Quantam Foam 9999"
        )
        self.artwork.save()
        
        # create a dummy image
        self.image = Image(
            legacy_id = 6350,
            title = "Quantam Foam 9999",
            slug = "quantam-foam-9999",
            ww_img = 'https://seattle.winstonwachter.com/wp-content/uploads/2020/02/Brown_QuantumFoam_2019_CutPaper_34x34inches_web.jpg'
        )
        self.image.save()

        # save the dummy image to the Artwork
        if self.artwork.work_images.exists():
            self.artwork.work_images.clear()
        self.artwork.work_images.set([self.image])
        self.artwork.save()

        # assign the artwork a featured image using the first work_image
        self.enhanced_artwork = Importer.make_artwork_featured_image_from_work_images( self.artwork )

    def test_artwork_has_work_images(self):
        native_image_ids = [self.image.legacy_id]
        artwork_image_ids = []
        if self.artwork.work_images.all():
            for i in self.artwork.work_images.all():
                artwork_image_ids.append(i.legacy_id)
        self.assertEqual( native_image_ids, artwork_image_ids ) 

    def test_artwork_enhanced(self):
        self.assertTrue(self.enhanced_artwork)

    def test_enhanced_artwork_has_featured_image(self):
        self.assertEqual( self.image, self.enhanced_artwork.featured_image )

class ConnectArtworkMediums(TestCase):

    # create dummy records
    def setUp(self):

        # query the artworks api
        data = Importer.query_api('artworks','https://seattle.winstonwachter.com/wp-json/ww-scour/v2/artwork')
        if not data: 
            raise Exception("Couldn't get data")

        # save the first result we got
        entry = data[0]
        self.api_record = entry

        # create the record
        created_record = Importer.add_update_artwork(entry)
        if not created_record: 
            raise Exception("Couldn't create the record")
        self.db_record = created_record

        # try to get a match in the media api
        attachment_data = Importer.query_api('media','https://seattle.winstonwachter.com/wp-json/wp/v2/media/' + str(self.db_record.legacy_id))
        if not attachment_data:
            raise Exception("Couldn't find a match for attachment with legacy_id " + str(self.db_record.legacy_id) + " in media api")

        self.updated_record_with_mediums = Importer.connect_artwork_to_wp_tax( self.db_record, attachment_data, 'medium' )
        self.updated_record_with_aspects = Importer.connect_artwork_to_wp_tax( self.db_record, attachment_data, 'aspect' )
        self.attachment_data = attachment_data
    
    def test_medium_setup_worked(self):
        this_record_tax_ids = []
        if self.updated_record_with_mediums.work_mediums.all():
            for t in self.updated_record_with_mediums.work_mediums.all():
                this_record_tax_ids.append(t.legacy_id)
        try: attachment_tax = self.attachment_data['ww-tax/mediums']
        except: attachment_tax = None
        if attachment_tax: attachment_tax_ids = attachment_tax
        else: attachment_tax_ids = []
        self.assertEqual( this_record_tax_ids, attachment_tax_ids )

    def test_aspect_setup_worked(self):
        this_record_tax_ids = []
        if self.updated_record_with_aspects.work_aspects.all():
            for t in self.updated_record_with_aspects.work_aspects.all():
                this_record_tax_ids.append(t.legacy_id)
        try: attachment_tax = self.attachment_data['ww-tax/aspects']
        except: attachment_tax = None
        if attachment_tax: attachment_tax_ids = attachment_tax
        else: attachment_tax_ids = []
        self.assertEqual( this_record_tax_ids, attachment_tax_ids )

    # loop through ww-tax/mediums
    # if the medium doesn't exist, throw an error
    # else connect the two
    # test that the test db entry has the same connections as the api record
    # (see test_created_record_has_correct_artists)

class ConnectArtworkArtists(TestCase):

    def setUp(self):

        self.artwork = Artwork(
            title = "Michael Schultheis | Blue Cycloids Trio"
        )
        self.artwork.save()

        self.artist = Artist(
            title = "Michael Schultheis"
        )
        self.artist.save()

        self.artwork_with_artist = Importer.get_artwork_artist_from_artwork_title( self.artwork )

    def test_artist_parsed(self):
        self.assertEqual( self.artist.title, self.artwork_with_artist.artist.title )


class ImageImport(TestCase):

    # test whether we can open query the api
    def test_query_api(self):
        data = Importer.query_api('media', api_root + 'media?media_type=image' )
        self.assertTrue(data)

    # create dummy records
    def setUp(self):

        data = Importer.query_api('media', api_root + 'media?media_type=image' )
        if not data: 
            raise Exception("Couldn't get data")

        entry = data[0]
        self.api_record = entry
        
        created_record = Importer.add_update_image(entry)
        if not created_record: 
            raise Exception("Couldn't create the record")
        self.db_record = created_record

    def test_created_record_id(self):
        self.assertEqual( self.db_record.legacy_id, self.api_record['id'] )

    def test_created_record_title(self):
        self.assertEqual( self.db_record.title, self.api_record['title']['rendered'].strip() )

    def test_created_record_slug(self):
        self.assertEqual( self.db_record.slug, self.api_record['slug'].strip() )

    def test_created_record_alt(self):
        self.assertEqual( self.db_record.alt, self.api_record['alt_text'].strip() )

    def test_created_record_caption(self):
        self.assertEqual( self.db_record.caption, self.api_record['caption']['rendered'].strip() )

    def test_created_record_ww_img(self):
        self.assertEqual( self.db_record.ww_img, self.api_record['source_url'] )

class ConnectImageFile(TestCase):

    # create dummy records
    def setUp(self):

        dummy_record = Image(
            legacy_id = 999999,
            title = "Test Image",
            slug = "test-image-999999",
            # ww_img = 'https://seattle.winstonwachter.com/wp-content/uploads/2019/07/Manitach_AllRightAsFuck_2019_Coloredpencilonpaper_22pt5x30pt25inches_web.jpg'
            ww_img = 'https://seattle.winstonwachter.com/wp-content/uploads/2020/05/Screen-Shot-2020-05-07-at-3.59.19-PM.png'
        )
        dummy_record.save()

        data = Importer.add_update_image_file_from_own_url(999999)
        if not data:
            raise Exception("Couldn't get data")

        self.db_record = data

    def test_has_image(self):
        self.assertTrue( self.db_record.img )

    def test_has_350(self):
        self.assertTrue( self.db_record.img.crop['350x350'] )

    def test_has_550(self):
        self.assertTrue( self.db_record.img.crop['550x550'] )

    def test_has_maxSquare(self):
        self.assertTrue( self.db_record.img.crop['1040x1040'] )

    def tearDown(self):
        self.db_record.img.delete_sized_images()
        if os.path.isfile(self.db_record.img.path):
            os.remove(self.db_record.img.path)

class VideoImport(TestCase):

    # test whether we can open query the api
    def test_query_api(self):
        data = Importer.query_api('videos')
        self.assertTrue( data )

    # test paginated query of all resources
    def test_query_api_all(self):
        data = Importer.query_api_all('videos')
        self.assertTrue( data )

    # create dummy records
    def setUp(self):

        data = Importer.query_api('videos')
        if not data: 
            raise Exception("Couldn't get data")

        entry = data[0]
        self.api_record = entry

        created_record = Importer.add_update_video(entry)
        if not created_record: 
            raise Exception("Couldn't create the record")
        self.db_record = created_record

    def test_created_record_id(self):
        self.assertEqual( self.db_record.legacy_id, self.api_record['id'] )

    def test_created_record_title(self):
        self.assertEqual( self.db_record.title, self.api_record['title']['rendered'] )

    # ensure that the description is blank
    def test_created_record_description(self):
        self.assertEqual( self.db_record.description, '' )

    def test_created_record_slug(self):
        self.assertEqual( self.db_record.slug, self.api_record['slug'] )

    def test_created_record_post_status(self):    
        if self.api_record['status'] == 'publish':
            self.assertEqual( self.db_record.post_status, 'published' )
        else:
            self.assertEqual( self.db_record.post_status, 'draft' )

    def test_created_video_url(self):
        try: api_video_url = self.api_record['acf']['ww_video_embed']
        except: api_video_url = None
        if api_video_url:
            self.assertEqual( self.db_record.video_url, api_video_url )
        else:
            self.assertIsNone( self.db_record.video_url )

class PostImport(TestCase):

    # test whether we can open query the api
    def test_query_api(self):
        data = Importer.query_api('posts','http://blog.winstonwachter.com/wp-json/wp/v2/posts?categories=642')
        self.assertTrue( data )

    # test paginated query of all resources
    def test_query_api_all(self):
        data = Importer.query_api_all('posts','http://blog.winstonwachter.com/wp-json/wp/v2/posts?categories=642')
        self.assertTrue( data )

    # create dummy records
    def setUp(self):

        data = Importer.query_api('posts','http://blog.winstonwachter.com/wp-json/wp/v2/posts?categories=642')
        if not data: 
            raise Exception("Couldn't get data")

        entry = data[0]
        self.api_record = entry

        created_record = Importer.add_update_post(entry)
        if not created_record: 
            raise Exception("Couldn't create the record")
        self.db_record = created_record

class ArtistImport(TestCase):

    # test whether we can open query the api
    def test_query_api(self):
        data = Importer.query_api('artists')
        self.assertTrue( data )

    # test paginated query of all resources
    def test_query_api_all(self):
        data = Importer.query_api_all('artists')
        self.assertTrue( data )

    # create dummy records
    def setUp(self):

        data = Importer.query_api('artists')
        if not data: 
            raise Exception("Couldn't get data")

        entry = data[0]
        self.api_record = entry

        created_record = Importer.add_update_artist(entry)
        if not created_record: 
            raise Exception("Couldn't create the record")
        self.db_record = created_record

    # test the dummy record's legacy id
    def test_created_record_id(self):
        self.assertEqual( self.db_record.legacy_id, self.api_record['id'] )

    def test_created_record_title(self):
        self.assertEqual( self.db_record.title, self.api_record['title']['rendered'] )

    def test_created_record_description(self):
        self.assertEqual( self.db_record.description, clean_html(self.api_record['content']['rendered'].strip()) )

    def test_created_record_slug(self):
        self.assertEqual( self.db_record.slug, self.api_record['slug'] )

    def test_created_record_post_status(self):    
        if self.api_record['status'] == 'publish':
            self.assertEqual( self.db_record.post_status, 'published' )
        else:
            self.assertEqual( self.db_record.post_status, 'draft' )

    def test_created_record_active_status(self):
        try: status = self.api_record['acf']['ww_artist_active_status']
        except: status = 'inactive'
        if status == 'active':
            self.assertEqual( self.db_record.active_status, 'active' )
        else:
            self.assertEqual( self.db_record.active_status, 'inactive' )

    def test_created_record_first_name(self):
        try: first_name = self.api_record['acf']['ww_artist_first_name']
        except: first_name = ''
        self.assertEqual( self.db_record.first_name, first_name )

    def test_created_record_last_name(self):
        try: last_name = self.api_record['acf']['ww_artist_last_name']
        except: last_name = ''
        self.assertEqual( self.db_record.last_name, last_name )

    def test_created_record_featured_image(self):
        try: api_featured_media = self.api_record['featured_media']
        except: api_featured_media = None
        if api_featured_media:
            self.assertEqual( self.db_record.featured_image.legacy_id, api_featured_media )
        else:
            self.assertFalse( self.db_record.featured_image )

class ArtistGlossary(TestCase):

    def setUp(self):
        self.data = Importer.add_update_artist_glossary()

    def test_we_have_data(self):
        self.assertTrue( self.data )

class ArtistConnectImages(TestCase):

    # create dummy records
    def setUp(self):

        # get the data from the api
        data = Importer.query_api('artists')
        if not data: 
            raise Exception("Couldn't get data")

        # pluck out a record for testing
        entry = data[0]
        self.api_record = entry

        # try to create a new record with the api entry we've saved
        created_record = Importer.add_update_artist(entry)
        if not created_record: 
            raise Exception("Couldn't create the record")

        # now try to connect our created record with its images
        self.db_record_with_work_images = Importer.connect_artist_to_images( created_record, self.api_record )
        self.db_record_with_install_images = Importer.connect_artist_to_images( created_record, self.api_record, 'ww_artist_installation_images' )

    def test_record_has_work_images(self):
        try: api_imgs = self.api_record['acf']['ww_artist_images']
        except: api_imgs = None
        if api_imgs:
            api_img_list = []
            for i in api_imgs:
                api_img_list.append(int(i['id']))
            api_img_list.sort()
            # print(api_img_list)
            record_img_list = []
            if self.db_record_with_work_images.work_images.exists():
                for i in self.db_record_with_work_images.work_images.all():
                    record_img_list.append( int(i.legacy_id) )
                record_img_list.sort()
                # print(record_img_list)
            self.assertEqual( record_img_list, api_img_list )
        else:
            self.assertFalse( self.db_record_with_work_images.work_images.exists() )

    def test_record_has_install_images(self):
        try: api_imgs = self.api_record['acf']['ww_artist_installation_images']
        except: api_imgs = None
        if api_imgs:
            api_img_list = []
            for i in api_imgs:
                api_img_list.append(int(i['id']))
            api_img_list.sort()
            # print(api_img_list)
            record_img_list = []
            if self.db_record_with_install_images.install_images.exists():
                for i in self.db_record_with_install_images.install_images.all():
                    record_img_list.append( int(i.legacy_id) )
                record_img_list.sort()
                # print(record_img_list)
            self.assertEqual( record_img_list, api_img_list )
        else:
            self.assertFalse( self.db_record_with_install_images.install_images.exists() )

class ExhibitionImport(TestCase):

    # test whether we can open query the api
    def test_query_api(self):
        data = Importer.query_api('exhibitions')
        self.assertTrue( data )

    # test paginated query of all resources
    def test_query_api_all(self):
        data = Importer.query_api_all('exhibitions')
        self.assertTrue( data )

    # create dummy records
    def setUp(self):

        data = Importer.query_api('exhibitions')
        if not data: 
            raise Exception("Couldn't get data")

        entry = data[0]
        self.api_record = entry

        created_record = Importer.add_update_exhibition(entry)
        if not created_record: 
            raise Exception("Couldn't create the record")
        self.db_record = created_record

    def test_created_record_id(self):
        self.assertEqual( self.db_record.legacy_id, self.api_record['id'] )

    def test_created_record_title(self):
        self.assertEqual( self.db_record.title, self.api_record['title']['rendered'] )

    def test_created_record_description(self):
        self.assertEqual( self.db_record.description, self.api_record['content']['rendered'].strip() )

    def test_created_record_slug(self):
        self.assertEqual( self.db_record.slug, self.api_record['slug'] )

    def test_created_record_post_status(self):    
        if self.api_record['status'] == 'publish':
            self.assertEqual( self.db_record.post_status, 'published' )
        else:
            self.assertEqual( self.db_record.post_status, 'draft' )

    def test_created_record_opening_date(self):
        try: opening = self.api_record['acf']['ww_exh_opening']
        except: opening = None
        if opening:
            opening_date = parser.parse(opening)
            opening_date = opening_date.date()
            self.assertEqual( self.db_record.open_date, opening_date )

    def test_created_record_closing_date(self):
        try: closing = self.api_record['acf']['ww_exh_closing']
        except: closing = None
        if closing:
            closing_date = parser.parse(closing)
            closing_date = closing_date.date()
            self.assertEqual( self.db_record.close_date, closing_date )

    def test_created_record_reception(self):
        try: reception = self.api_record['acf']['ww_exh_opening_event_date']
        except: reception = None
        if reception:
            reception = reception.strip()
            self.assertEqual( self.db_record.reception, reception )

    def test_created_record_artist_attendance(self):
        try: artist_in_attendance = self.api_record['acf']['ww_exh_artist_attending']
        except: artist_in_attendance = None
        if artist_in_attendance and artist_in_attendance == 'yes':
            self.assertTrue( self.db_record.artist_in_attendance )
        else:
            self.assertFalse( self.db_record.artist_in_attendance )

    def test_created_record_featured_image(self):
        try: api_featured_media = self.api_record['featured_media']
        except: api_featured_media = None
        if api_featured_media:
            self.assertEqual( self.db_record.featured_image.legacy_id, api_featured_media )
        else:
            self.assertFalse( self.db_record.featured_image )

class ExhibitionConnectArtists(TestCase):

    def setUp(self):

        # get the data from the api
        data = Importer.query_api('exhibitions')
        if not data: 
            raise Exception("Couldn't get data")

        # pluck out a record for testing
        entry = data[1]
        self.api_record = entry

        # try to create a new record with the api entry we've saved
        self.db_record = Importer.add_update_exhibition(entry)
        if not self.db_record: 
            raise Exception("Couldn't create the record")

    def test_record_has_correct_artists(self):
        
        try: api_artists = self.api_record['acf']['ww_exh_artists']
        except: api_artists = None

        if api_artists:
            api_artist_list = []
            for i in api_artists:
                api_artist_list.append(int(i['ID']))
            api_artist_list.sort()
            # print(api_artist_list)
            record_artist_list = []
            if self.db_record.artists.exists():
                for i in self.db_record.artists.all():
                    record_artist_list.append( int(i.legacy_id) )
                record_artist_list.sort()
                # print(record_artist_list)
            self.assertEqual( record_artist_list, api_artist_list )
        else:
            self.assertFalse( self.db_record.artists.exists() )

class ExhibitionConnectImages(TestCase):

    # create dummy records
    def setUp(self):

        # get the data from the api
        data = Importer.query_api('exhibitions')
        if not data: 
            raise Exception("Couldn't get data")

        # pluck out a record for testing
        entry = data[0]
        self.api_record = entry

        # try to create a new record with the api entry we've saved
        created_record = Importer.add_update_exhibition(entry)
        if not created_record: 
            raise Exception("Couldn't create the record")

        # now try to connect our created record with its images
        self.db_record = Importer.connect_exhibition_to_images( created_record, self.api_record )

    def test_record_has_correct_images(self):

        try: api_imgs = self.api_record['acf']['ww_exh_images']
        except: api_imgs = None

        if api_imgs:
            api_img_list = []
            for i in api_imgs:
                api_img_list.append(int(i['id']))
            api_img_list.sort()
            # print(api_img_list)
            record_img_list = []
            if self.db_record.images.exists():
                for i in self.db_record.images.all():
                    record_img_list.append( int(i.legacy_id) )
                record_img_list.sort()
                # print(record_img_list)
            self.assertEqual( record_img_list, api_img_list )
        else:
            self.assertFalse( self.db_record.images.exists() )

class ContactImport(TestCase):

    # test that the document columns match our key
    def test_source_integrity(self):

        doc_key = {
            '5': 'Company',
            '11': 'Fax',
            '12': 'First name',
            '16': 'Last name',
            '17': 'Notes',
            '21': 'Salutation',
            '23': 'Title'
        }

        # ensure the import of the document
        data = Importer.import_filemaker_file( 'contacts' )
        self.assertTrue( data )

        # take the first row of the doc and turn it into an array
        header = next(data)
        header_arr = header.rstrip().split('|')

        # test that the header names match the document key above
        for key, value in doc_key.items():
            self.assertEqual( value, header_arr[int(key)] )

class ReadXML(TestCase):

    def setUp(self):
        filename = 'contacts'
        self.data = Importer.parse_xml_import_file(filename)
        self.csvFile = Importer.write_xml_to_csv(self.data,filename)

    def test_read_xml(self):
        self.assertTrue(self.data)

    def test_wrote_csv(self):
        self.assertTrue(self.csvFile)

class MergeInventoryContacts(TestCase):
    
    def setUp(self):
        self.data = Importer.merge_inventory_contacts()

    def test_worked(self):
        self.assertTrue(self.data)

class BulkRunHoudiniAPI(TestCase):

    def setUp(self):

        # create a test Image record
        self.imageRecord1 = Image(
            title = "Quantam Foam",
            slug = "quantam-foam",
            legacy_id = 6350,
            ww_img = "https://seattle.winstonwachter.com/wp-content/uploads/2020/02/Brown_QuantumFoam_2019_CutPaper_34x34inches_web.jpg"
        )
        self.imageRecord1.save()

        # give the test record an actual image (using the wp image)
        self.imageRecord1 = Importer.add_update_image_file_from_own_url(self.imageRecord1.legacy_id)

        # create a test Artwork record
        self.artworkRecord1 = Artwork(
            title = "Quantam Foam"
        )
        self.artworkRecord1.save()

        # connect the Image and Artwork records
        self.intermediary1 = ArtworkImage(
            artwork = self.artworkRecord1,
            image = self.imageRecord1,
            order=1
        )
        self.intermediary1.save()
        self.artworkRecord1.save()

        # create another test Image record (but without a ww_img attribute)
        self.imageRecord2 = Image(
            title = "Blue Cycloids Trio",
            slug = "asjsklajj"
        )
        self.imageRecord2.save()

        # create another test Artwork record
        self.artworkRecord2 = Artwork(
            title = "Blue Cycloids Trio",
            slug = "asdfjkl"
        )
        self.artworkRecord2.save()

        # connect the Image and Artwork records
        self.intermediary2 = ArtworkImage(
            artwork = self.artworkRecord2,
            image = self.imageRecord2,
            order=1
        )
        self.intermediary2.save()
        self.artworkRecord2.save()

        self.artworks_to_process = Importer.call_houdini_api_for_all_artwork_images()

    def tearDown(self):
        self.imageRecord1.img.delete_sized_images()

    def test_image_record_1_was_created(self):
        self.assertTrue( self.imageRecord1 )

    def test_artwork_record_1_was_created(self):
        self.assertTrue( self.artworkRecord1 )

    def test_artwork_record_1_has_images(self):
        self.assertTrue( self.artworkRecord1.work_images )

class CheckMasterInventoryDataCompleteness(TestCase):

    def setUp(self):

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-inventory', listify = True )

    def test_all_rows_have_all_columns(self):

        # test that all rows have same number of columns as header
        if self.data:
            i = 1
            header_col_count = len(self.data[0])
            for row in self.data:
                row = Importer.clean_quoted_csv_row(row)
                self.assertEqual( header_col_count, len(row) )
                i = i + 1

class ImportArtistsFromMasterInventory(TestCase):

    def setUp(self):

        # get the column keys necessary for this import
        self.doc_key = Importer.get_keys_for_master_inventory_file()

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-inventory', listify = True )
        self.data_num_rows = len(self.data)

        # grab the header row and a random row, for testing
        self.header_row = self.data.pop(0)
        self.test_row = self.data[2860]

        # create a test artist using the test row
        self.created_artist = Importer.import_artist_from_spreadsheet_row( self.test_row )

        # use the data to run the bulk import function
        self.records_bulk_imported = Importer.import_artists_from_spreadsheet(self.data)

    # test that the document columns match our key
    def test_source_integrity(self):
        for key, value in self.doc_key.items():
            self.assertEqual( value['column_name'], self.header_row[value['column_index']] )

    def test_artist_created_from_spreadsheet_row(self):
        self.assertTrue( self.created_artist )

    def test_created_artist_record_has_right_values(self):
        self.assertEqual( self.created_artist.first_name, self.test_row[self.doc_key['first_name']['column_index']] )
        self.assertEqual( self.created_artist.last_name, self.test_row[self.doc_key['last_name']['column_index']] )
        self.assertEqual( self.created_artist.legacy_id, self.test_row[self.doc_key['legacy_id']['column_index']] )

    def check_total_num_artists_imported(self):
        self.assertEqual( ( len(self.data) - 1 ), self.records_bulk_imported )

class CheckMasterContactsDataCompleteness(TestCase):

    def setUp(self):

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-contacts', listify = True )

    def test_all_rows_have_all_columns(self):

        # test that all rows have same number of columns as header
        if self.data:
            i = 1
            header_col_count = len(self.data[0])
            for row in self.data:
                print(row[0])
                # print(row)
                row = Importer.clean_quoted_csv_row(row)
                self.assertEqual( header_col_count, len(row) )
                i = i + 1

# this was a one-time deal, for populating a column of the spreadsheet
'''class FigureOutContactArtistPreferences(TestCase):

    def setUp(self):
        self.artists = Importer.import_artists_from_spreadsheet()
        self.data = Importer.figure_out_contact_artist_preferences()

    def test_whatever(self):
        return True'''

class ImportContactCategories(TestCase):

    def setUp(self):

        self.source_data = Importer.import_filemaker_file( 'contact-categories', listify = True )

        self.categories = Importer.create_update_contact_categories_from_spreadsheet()

    def test_categories_created(self):
        self.assertEqual( len(self.categories), len(self.source_data) )

class ImportContactsFromMasterContacts(TestCase):

    def setUp(self):

        # get the column keys necessary for this import
        self.doc_key = Importer.get_spreadsheet_keys_for_contact_import()

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-contacts', listify = True )
        self.data_num_rows = len(self.data)

        # grab the header row and a random row, for testing
        self.header_row = self.data.pop(0)
        self.test_row = self.data[12000]

        # create a test Contact using the test row
        self.created_record = Importer.import_update_contact_basics_from_spreadsheet_row( self.test_row )

        # use the data to run the bulk import function
        self.records_bulk_imported = Importer.import_update_contact_basics_from_spreadsheet(self.data)

    # test that the document columns match our key
    def test_source_integrity(self):
        for key, value in self.doc_key.items():
            self.assertEqual( value['column_name'], self.header_row[value['column_index']] )

    def test_artist_created_from_spreadsheet_row(self):
        self.assertTrue( self.created_record )

    def test_created_artist_record_has_right_values(self):
        self.assertEqual( self.created_record.legacy_id, self.test_row[self.doc_key['legacy_id']['column_index']] )
        self.assertEqual( self.created_record.first_name, self.test_row[self.doc_key['first_name']['column_index']] )
        self.assertEqual( self.created_record.last_name, self.test_row[self.doc_key['last_name']['column_index']] )
        self.assertEqual( self.created_record.position, self.test_row[self.doc_key['position']['column_index']] )
        self.assertEqual( self.created_record.salutation, self.test_row[self.doc_key['salutation']['column_index']] )
        self.assertEqual( self.created_record.company, self.test_row[self.doc_key['company']['column_index']] )
        self.assertEqual( self.created_record.admin_notes, self.test_row[self.doc_key['admin_notes']['column_index']] )
        
    def test_total_num_records_imported(self):
        self.assertEqual( ( len(self.data) ), self.records_bulk_imported )

class ImportAddressesFromMasterContacts(TestCase):

    def setUp(self):

        # get the column keys necessary for this import
        self.doc_key = Importer.get_spreadsheet_keys_for_address_import()

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-contacts', listify = True )
        self.data_num_rows = len(self.data)

        # grab the header row and a random row, for testing
        self.header_row = self.data.pop(0)
        self.test_row = self.data[3]

        # create a test record using the test row
        self.created_record = Importer.import_update_address_from_spreadsheet_row( self.test_row )

        # use the data to run the bulk import function
        self.records_bulk_imported = Importer.import_update_addresses_from_spreadsheet(self.data)

    # test that the document columns match our key
    def test_source_integrity(self):
        for key, value in self.doc_key.items():
            self.assertEqual( value['column_name'], self.header_row[value['column_index']] )

    def test_record_created_from_spreadsheet_row(self):
        self.assertTrue( self.created_record )

    def test_created_record_has_right_values(self):
        self.assertEqual( self.created_record.legacy_id, self.test_row[self.doc_key['legacy_id']['column_index']] )
        self.assertEqual( self.created_record.contact.legacy_id, self.test_row[self.doc_key['legacy_id']['column_index']] )
        self.assertEqual( self.created_record.address_1, self.test_row[self.doc_key['address_1']['column_index']] )
        self.assertEqual( self.created_record.address_2, self.test_row[self.doc_key['address_2']['column_index']] )
        self.assertEqual( self.created_record.city, self.test_row[self.doc_key['city']['column_index']] )
        self.assertEqual( self.created_record.state, self.test_row[self.doc_key['state']['column_index']] )
        self.assertEqual( self.created_record.zipcode, self.test_row[self.doc_key['zipcode']['column_index']] )
        self.assertEqual( self.created_record.country, self.test_row[self.doc_key['country']['column_index']] )

    def test_total_num_records_imported(self):
        self.assertEqual( 12723, self.records_bulk_imported )

class ImportPhonesFromMasterContacts(TestCase):

    def setUp(self):

        # get the column keys necessary for this import
        self.doc_key = Importer.get_spreadsheet_keys_for_telephone_import()

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-contacts', listify = True )
        self.data_num_rows = len(self.data)

        # grab the header row and a random row, for testing
        self.header_row = self.data.pop(0)
        self.test_row = self.data[62]

        # create a test record using the test row
        self.work_record = Importer.import_telephone_from_spreadsheet_row( self.test_row, number_type='work' )
        self.home_record = Importer.import_telephone_from_spreadsheet_row( self.test_row, number_type='home' )
        self.other_record = Importer.import_telephone_from_spreadsheet_row( self.test_row, number_type='other' )

        # use the data to run the bulk import function
        # self.work_records_bulk_imported = Importer.import_telephones_from_spreadsheet( self.data, phone_type = 'work' )
        # self.home_records_bulk_imported = Importer.import_telephones_from_spreadsheet( self.data, phone_type = 'home' )
        # self.other_records_bulk_imported = Importer.import_telephones_from_spreadsheet( self.data, phone_type = 'other' )

        self.all_records_bulk_imported = Importer.import_telephones_from_spreadsheet()

    # test that the document columns match our key
    def test_source_integrity(self):
        for key, value in self.doc_key.items():
            self.assertEqual( value['column_name'], self.header_row[value['column_index']] )

    def test_record_created_from_spreadsheet_row(self):
        self.assertTrue( self.work_record )
        self.assertTrue( self.home_record )
        self.assertTrue( self.other_record )

    def test_created_record_has_right_values(self):
        
        self.assertEqual( self.work_record.number, self.test_row[self.doc_key['work_number']['column_index']] )
        self.assertEqual( self.work_record.note, self.test_row[self.doc_key['work_number_note']['column_index']] )
        self.assertEqual( self.work_record.contact.legacy_id, self.test_row[self.doc_key['legacy_id']['column_index']] )
        self.assertTrue( self.work_record.primary )

        self.assertEqual( self.home_record.number, self.test_row[self.doc_key['home_number']['column_index']] )
        self.assertEqual( self.home_record.note, self.test_row[self.doc_key['home_number_note']['column_index']] )
        self.assertEqual( self.home_record.contact.legacy_id, int(self.test_row[self.doc_key['legacy_id']['column_index']]) )
        self.assertFalse( self.home_record.primary )

        self.assertEqual( self.other_record.number, self.test_row[self.doc_key['other_number']['column_index']] )
        self.assertEqual( self.other_record.note, self.test_row[self.doc_key['other_number_note']['column_index']] )
        self.assertEqual( self.other_record.contact.legacy_id, int(self.test_row[self.doc_key['legacy_id']['column_index']]) )
        self.assertFalse( self.other_record.primary )

    def test_total_num_records_imported(self):
        self.assertEqual( 4580, self.work_records_bulk_imported )
        self.assertEqual( 2148, self.home_records_bulk_imported )
        self.assertEqual( 1487, self.other_records_bulk_imported )

class ImportEmailsFromMasterContacts(TestCase):

    def setUp(self):

        # get the column keys necessary for this import
        self.doc_key = Importer.get_spreadsheet_keys_for_email_import()

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-contacts', listify = True )
        self.data_num_rows = len(self.data)

        # grab the header row and some test rows
        self.header_row = self.data.pop(0)
        self.test_row_with_single = self.data[610]
        self.test_row_with_multiple = self.data[480]
        self.test_row_empty = self.data[0]

        # create some test records
        self.single_record = Importer.import_email_from_spreadsheet_row( self.test_row_with_single )
        self.multi_record = Importer.import_email_from_spreadsheet_row( self.test_row_with_multiple )
        self.empty_record = Importer.import_email_from_spreadsheet_row( self.test_row_empty )

        # now test the bulk import
        self.all_records_bulk_imported = Importer.import_emails_from_spreadsheet()

    # test that the document columns match our key
    def test_source_integrity(self):
        for key, value in self.doc_key.items():
            self.assertEqual( value['column_name'], self.header_row[value['column_index']] )

    # make sure our test rows have the values we're testing for
    def test_rows_fit_criteria(self):
        self.assertNotEqual( self.test_row_with_single[17].strip(), '' )
        self.assertEqual( len(self.test_row_with_multiple[17].rstrip().split(',')), 2 )
        self.assertEqual( self.test_row_empty[17].strip(), '' )

    def test_correct_num_records_created(self):
        self.assertEqual( len(self.single_record), 1 )
        self.assertEqual( len(self.multi_record), 2 )
        self.assertFalse( self.empty_record )

    def test_records_have_correct_attributes(self):
        self.assertEqual( self.single_record[0].address, self.test_row_with_single[self.doc_key['address']['column_index']] )
        self.assertEqual( self.single_record[0].contact.legacy_id, self.test_row_with_single[self.doc_key['legacy_id']['column_index']] )

    def test_total_num_records_imported(self):
        self.assertEqual( 6603, self.all_records_bulk_imported )

class ConnectContactArtistPreferences(TestCase):

    def setUp(self):

        # get the column keys necessary for this import
        self.doc_key = Importer.get_spreadsheet_keys_for_contact_artists()

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-contacts', listify = True )
        self.data_num_rows = len(self.data)

        # grab the header row and some test rows
        self.header_row = self.data.pop(0)
        self.test_row_with_single = self.data[7]
        self.test_row_with_multiple = self.data[10]
        self.test_row_empty = self.data[0]

        # import all artists into test database
        self.artist_data = Importer.import_filemaker_file( 'master-inventory', listify = True )
        self.artist_header_row = self.artist_data.pop(0)
        self.artist_records = Importer.import_artists_from_spreadsheet(self.artist_data)

        # create some test records
        self.single_record = Importer.import_contact_artist_preference_from_spreadsheet_row( self.test_row_with_single )
        self.multi_record = Importer.import_contact_artist_preference_from_spreadsheet_row( self.test_row_with_multiple )
        self.empty_record = Importer.import_contact_artist_preference_from_spreadsheet_row( self.test_row_empty )

        # now test the bulk import
        self.all_records = Importer.import_contact_artist_preferences_from_spreadsheet(self.data)

    # test that the document columns match our key
    def test_source_integrity(self):
        for key, value in self.doc_key.items():
            self.assertEqual( value['column_name'], self.header_row[value['column_index']] )

    # make sure our test rows have the values we're testing for
    def test_rows_fit_criteria(self):
        self.assertNotEqual( self.test_row_with_single[24].rstrip().split('/'), '' )
        self.assertEqual( len(self.test_row_with_multiple[24].rstrip().split('/')), 2 )
        self.assertEqual( self.test_row_empty[24].strip(), '' )

    def test_correct_num_records_created(self):
        self.assertEqual( len(self.single_record.artists.all()), 1 )
        self.assertEqual( len(self.multi_record.artists.all()), 2 )
        self.assertFalse( self.empty_record )

    def test_total_num_records_imported(self):
        self.assertEqual( 7082, self.all_records )

class ConnectContactCategories(TestCase):

    def setUp(self):

        # get the column keys necessary for this import
        self.doc_key = Importer.get_spreadsheet_keys_for_contact_categories()

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-contacts', listify = True )
        self.data_num_rows = len(self.data)

        # grab the header row and some test rows
        self.header_row = self.data.pop(0)
        self.test_row_with_single = self.data[0]
        self.test_row_with_multiple = self.data[10]
        self.test_row_empty = self.data[1]

        # import all categories into test database
        self.category_data = Importer.create_update_contact_categories_from_spreadsheet()

        # create some test records
        self.single_record = Importer.import_contact_category_from_spreadsheet_row( self.test_row_with_single )
        self.multi_record = Importer.import_contact_category_from_spreadsheet_row( self.test_row_with_multiple )
        self.empty_record = Importer.import_contact_artist_preference_from_spreadsheet_row( self.test_row_empty )

        # now test the bulk import
        self.all_records = Importer.import_contact_categories_from_spreadsheet(self.data)

    # test that the document columns match our key
    def test_source_integrity(self):
        for key, value in self.doc_key.items():
            self.assertEqual( value['column_name'], self.header_row[value['column_index']] )

    # make sure our test rows have the values we're testing for
    def test_rows_fit_criteria(self):
        self.assertEqual( len(self.test_row_with_single[self.doc_key['categories']['column_index']].rstrip().split('/')), 1 )
        self.assertEqual( len(self.test_row_with_multiple[self.doc_key['categories']['column_index']].rstrip().split('/')), 2 )
        self.assertEqual( self.test_row_empty[self.doc_key['categories']['column_index']].strip(), '' )

    def test_correct_num_records_created(self):
        self.assertEqual( len(self.single_record.categories.all()), 1 )
        self.assertEqual( len(self.multi_record.categories.all()), 2 )
        self.assertFalse( self.empty_record )

    def test_total_num_records_imported(self):
        self.assertEqual( 6080, self.all_records )

class ArtworkImportFromSpreadsheet(TestCase):

    def setUp(self):

        # get the column keys necessary for this import
        self.doc_key = Importer.get_spreadsheet_keys_for_artwork_import()

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-inventory', listify = True )

        # grab the header row and some test rows
        self.header_row = self.data.pop(0)
        self.test_row_sold = self.data[7488]
        self.test_row_removed = self.data[5739]
        self.test_row_no_wp = self.data[7486]
        self.test_row_successful = self.data[5687]
        self.test_row_bought = self.data[1]

        # run the artist import (a dependency of testing the artwork import)
        Importer.import_artists_from_spreadsheet(self.data)

        # run the artwork type import (a dependency fo testing the artwork import)
        Importer.import_artwork_types_from_spreadsheet()

        # try to create records using our test rows
        self.record_sold = Importer.add_update_artwork_from_spreadsheet_row( self.test_row_sold )
        self.record_removed = Importer.add_update_artwork_from_spreadsheet_row( self.test_row_removed )
        self.record_no_wp = Importer.add_update_artwork_from_spreadsheet_row( self.test_row_no_wp )
        self.record_successful = Importer.add_update_artwork_from_spreadsheet_row( self.test_row_successful )
        self.record_bought = Importer.add_update_artwork_from_spreadsheet_row( self.test_row_bought )

        # self.bulk_records = Importer.add_update_artworks_from_spreadsheet( self.data) 

    # test that the document columns match our key
    def test_source_integrity(self):
        for key, value in self.doc_key.items():
            self.assertEqual( value['column_name'], self.header_row[value['column_index']] )

    # make sure our test rows have the values we're testing for
    def test_rows_fit_criteria(self):
        self.assertEqual( self.test_row_sold[self.doc_key['status']['column_index']].strip(), 'Sold' )
        self.assertEqual( self.test_row_removed[self.doc_key['status']['column_index']].strip(), 'Removed' )
        self.assertEqual( self.test_row_no_wp[self.doc_key['legacy_id']['column_index']].strip(), '' )
        self.assertNotEqual( self.test_row_successful[self.doc_key['legacy_id']['column_index']].strip(), '' )
        self.assertNotEqual( self.test_row_bought[self.doc_key['bought_date']['column_index']].strip(), '' )

    '''def test_invalid_rows_got_skipped(self):
        self.assertFalse( self.record_sold )
        self.assertFalse( self.record_removed )
        self.assertFalse( self.record_no_wp )'''

    def test_success_record_has_right_fields(self):
        self.assertEqual( self.record_successful.title, self.test_row_successful[self.doc_key['title']['column_index']].strip() )
        self.assertEqual( self.record_successful.fm_id, self.test_row_successful[self.doc_key['fm_id']['column_index']].strip() )
        self.assertEqual( self.record_successful.year, self.test_row_successful[self.doc_key['year']['column_index']].strip() )
        self.assertEqual( self.record_successful.artist.legacy_id, int(self.test_row_successful[self.doc_key['artist_id']['column_index']].strip()) )
        self.assertEqual( len(self.record_bought.history.all()), 1 )
        record_work_types = []
        if self.record_successful.work_types.all().exists():    
            for type_obj in self.record_successful.work_types.all():
                record_work_types.append(int(type_obj.legacy_id))
        self.assertEqual( record_work_types, [int(self.test_row_successful[self.doc_key['type_id']['column_index']].strip())] )

class ArtworkCreateBoughtNote(TestCase):

    def setUp(self):

        # get the column keys necessary for this import
        self.doc_key = Importer.get_spreadsheet_keys_for_artwork_import()

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-inventory', listify = True )

        # grab the header row and some test rows
        self.header_row = self.data.pop(0)
        self.row_complete = self.data[11]
        self.row_missing_price = self.data[1]
        self.row_missing_from_price = self.data[261]
        self.invalid_row = self.data[6]

        self.record_complete = Importer.add_update_artwork_from_spreadsheet_row( self.row_complete )
        self.record_missing_price = Importer.add_update_artwork_from_spreadsheet_row( self.row_missing_price )
        self.record_missing_from_price = Importer.add_update_artwork_from_spreadsheet_row( self.row_missing_from_price )

        self.message_complete = Importer.maybe_add_update_artwork_bought_note( self.record_complete, self.row_complete )
        self.message_missing_price = Importer.maybe_add_update_artwork_bought_note( self.record_missing_price, self.row_missing_price )
        self.message_missing_from_price = Importer.maybe_add_update_artwork_bought_note( self.record_missing_from_price, self.row_missing_from_price )

    def test_notes(self):
        self.assertEqual( self.message_complete.description, "Bought from Pace Editions for $3600 on 5/15/1998." )
        self.assertEqual( self.message_missing_price.description, "Bought from Galerie Academia, Salzburg on 8/4/1997." )
        self.assertEqual( self.message_missing_from_price.description, "Bought on 9/2/2015." )

class ArtworkCreateInvoiceNote(TestCase):

    def setUp(self):

        # get the column keys necessary for this import
        self.doc_key = Importer.get_spreadsheet_keys_for_artwork_import()

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-inventory', listify = True )

        # grab the header row and some test rows
        self.header_row = self.data.pop(0)
        self.row_success = self.data[8]

        # create an Artwork record
        self.record_success = Importer.add_update_artwork_from_spreadsheet_row( self.row_success )

        # create a Moment record using the Artwork record
        self.message_success = Importer.maybe_add_update_artwork_invoice_note( self.record_success, self.row_success )

    def test_notes(self):
        self.assertEqual( self.message_success.description, "FileMaker Invoice No. 103" )

class ArtworkCreateRemovedNote(TestCase):

    def setUp(self):

        # get the column keys necessary for this import
        self.doc_key = Importer.get_spreadsheet_keys_for_artwork_import()

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-inventory', listify = True )

        # grab the header row and some test rows
        self.header_row = self.data.pop(0)
        self.row_success = self.data[0]

        # create an Artwork record
        self.record_success = Importer.add_update_artwork_from_spreadsheet_row( self.row_success )

        # create a Moment record using the Artwork record
        self.message_success = Importer.maybe_add_update_artwork_removed_note( self.record_success, self.row_success )

    def test_notes(self):
        self.assertEqual( self.message_success.description, "Removed from inventory 2000-01-11" )

class ArtworkCreateSoldNote(TestCase):

    def setUp(self):

        # get the column keys necessary for this import
        self.doc_key = Importer.get_spreadsheet_keys_for_artwork_import()

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-inventory', listify = True )

        # grab the header row and some test rows
        self.header_row = self.data.pop(0)
        self.row_missing_id = self.data[443]
        self.row_complete = self.data[8]

        # bulk-import Contacts, so they'll be available in tests below
        Importer.import_update_contact_basics_from_spreadsheet()

        # create an Artwork record
        self.record_missing_id = Importer.add_update_artwork_from_spreadsheet_row( self.row_missing_id )
        self.record_complete = Importer.add_update_artwork_from_spreadsheet_row( self.row_complete )

        # create a Moment record using the Artwork record
        self.message_missing_id = Importer.maybe_add_update_artwork_sold_note( self.record_missing_id, self.row_missing_id )
        self.message_complete = Importer.maybe_add_update_artwork_sold_note( self.record_complete, self.row_complete )

    def test_notes(self):
        self.assertEqual( self.message_missing_id.description, "Sold to Strom, Mark" )
        self.assertEqual( self.message_complete.description, "Sold to Audino, Tony" )
        self.assertEqual( int(self.message_complete.contact.legacy_id), int(self.row_complete[self.doc_key['buyer_id']['column_index']].strip()) )

class ImportConsignments(TestCase):

    def setUp(self):

        # get the column keys necessary for this import
        self.doc_key = Importer.get_spreadsheet_keys_for_consignment_import()

        # get the data from the spreadsheet
        self.data = Importer.import_filemaker_file( 'master-consignments-test', listify = True )
        self.data_full = Importer.import_filemaker_file( 'master-consignments', listify = True )

        # grab the header row and some test rows
        self.header_row = self.data.pop(0)
        self.row_primary = self.data[0]
        self.row_line_item = self.data[1]

        # import contacts (so that we can search them and connect clients to consignments)
        contact_data = Importer.import_filemaker_file( 'master-contacts-test', listify = True )
        contact_header_row = contact_data.pop(0)
        Importer.import_update_contact_basics_from_spreadsheet( contact_data )

        # import artworks (so that we can search them and connect artworks to consignments)
        artwork_data = Importer.import_filemaker_file( 'master-inventory-test', listify = True )
        artwork_header_row = artwork_data.pop(0)
        Importer.add_update_artworks_from_spreadsheet( artwork_data )

        # create some test records
        self.record_primary = Importer.add_update_consignment( self.row_primary )
        self.record_line_item = Importer.add_update_consignment( self.row_line_item )

    # test that the document columns match our key
    def test_source_integrity(self):
        for key, value in self.doc_key.items():
            self.assertEqual( value['column_name'], self.header_row[value['column_index']] )

    def test_all_rows_have_all_columns(self):

        # test that all rows have same number of columns as header
        if self.data:
            i = 1
            header_col_count = len(self.header_row)
            for row in self.data_full:
                row = Importer.clean_quoted_csv_row(row)
                self.assertEqual( header_col_count, len(row) )
                i = i + 1

    def test_has_right_values(self):
        self.assertEqual( self.record_primary.title, "Smith, Joel" )
        self.assertEqual( self.record_primary.commission, "This is the commission" )
        self.assertEqual( self.record_primary.admin_notes, "These are the admin notes" )
        self.assertEqual( self.record_primary.title, self.record_line_item.title )
        self.assertEqual( self.record_primary.commission, self.record_line_item.commission )
        self.assertEqual( self.record_primary.admin_notes, self.record_line_item.admin_notes )
        self.assertEqual( self.record_primary.client.legacy_id, int(self.row_primary[self.doc_key['client_id']['column_index']].strip()) )

class ImportInvoiceBasics(TestCase):

    def setUp(self):

        # get the column keys necessary for this import
        self.doc_key = Importer.get_spreadsheet_keys_for_invoice_import()

        # get the data from the spreadsheet
        # self.data = Importer.import_filemaker_file( 'master-invoices-test', listify = True )
        self.data = Importer.import_filemaker_file( 'master-invoices-test', listify = True )

        # grab the header row and some test rows
        self.header_row = self.data.pop(0)
        self.row_primary = self.data[0]
        self.row_line_item = self.data[2]

        # import contacts (so that we can search them and connect clients to consignments)
        contact_data = Importer.import_filemaker_file( 'master-contacts', listify = True )
        contact_header_row = contact_data.pop(0)
        Importer.import_update_contact_basics_from_spreadsheet( contact_data )

        # import artworks (so that we can search them and connect artworks to consignments)
        artwork_data = Importer.import_filemaker_file( 'master-inventory', listify = True )
        artwork_header_row = artwork_data.pop(0)
        Importer.add_update_artworks_from_spreadsheet( artwork_data )

        # create some test records
        self.record_primary = Importer.add_update_invoice( self.row_primary )
        self.record_line_item = Importer.add_update_invoice( self.row_line_item )

    # test that the document columns match our key
    def test_source_integrity(self):
        for key, value in self.doc_key.items():
            self.assertEqual( value['column_name'], self.header_row[value['column_index']] )

    # test that all rows have same number of columns as header
    def test_all_rows_have_all_columns(self):
        if self.data:
            i = 1
            header_col_count = len(self.header_row)
            for row in self.data:
                row = Importer.clean_quoted_csv_row(row)
                self.assertEqual( header_col_count, len(row) )
                i = i + 1

    def test_has_right_values(self):
        # print(self.record_line_item)
        self.assertEqual( int(self.record_primary.fm_id), 10 )
        self.assertEqual( self.record_primary.client.legacy_id, 3622 )
        self.assertEqual( int(self.record_primary.total), 14118 )
        self.assertEqual( self.record_primary.admin_notes, "Original invoice: CS-1700 $5,800 {LESS 10%= 5220} + TAX = $5,679.36 (Total amt pd). THIS INVOICE WAS CANCELLED, CLIENT PURCH’D LRGR PIECE-SEE INV #580. Line item: CS-165" )
        self.assertEqual( int(self.record_line_item.fm_id), 10 )