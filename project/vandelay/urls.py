from django.urls import path, include

from . admin import vandelay_admin

from . import views

app_name = 'vandelay'
urlpatterns = [
    path('', vandelay_admin.urls, name='vandelay_admin' ),
]