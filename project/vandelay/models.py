from django.db import models
from django.utils import timezone
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from lxml.html.clean import clean_html
import datetime
import requests
import os
import json
import csv
import xml.etree.ElementTree as ET 


from django.core.exceptions import ObjectDoesNotExist

from project.docent.models import Video, Post, Artist, ArtistImage, ArtistInstallImage, Exhibition, ExhibitionImage, Artwork, ArtworkVariation, ArtworkImage, ArtworkMedium, ArtworkAspect, ArtworkType, ArtworkLocation, Image, Contact, ContactCategory, Address, Telephone, Email, Moment, Consignment, ConsignmentWork, Invoice

from project.utils.helpers import *

from django.urls import reverse

from dateutil import parser
from django.utils.timezone import make_aware
from django.utils.dateparse import parse_datetime

from django.http import HttpResponse, HttpResponseRedirect

# for reading and parsing image files
from django.core.files import File
from urllib.parse import urlparse
from PIL import Image as Pillbug
from django.utils.six import BytesIO

api_root = 'https://seattle.winstonwachter.com/wp-json/wp/v2/'
# api_root = 'https://wwseadev.wpengine.com/wp-json/wp/v2/'

class Importer(models.Model):

    class Meta:
        abstract = True

    # =======================
    # BASE FUNCTIONS
    # =======================

    # the base, low-level api-caller
    def get_wp(path):
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
        response = requests.get(path,headers=headers)
        if response.status_code != 200:
            return None
        else:
            # print(response.headers['x-wp-total'])
            return response.json()

    # a basic way to call a given wordpress api
    def query_api( content_type, url_override = False ):
        if url_override:
            return Importer.get_wp( url_override )
        else:
            return Importer.get_wp( api_root + content_type )

    # get the total number of results for a given wp api resource
    def get_resource_result_count( resource_url ):
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
        response = requests.get(resource_url,headers=headers)
        if response.status_code != 200:
            return 0
        else:
            return response.headers['x-wp-total']

    # an iterator, capable of calling query_api for all avaiable resources
    def query_api_all( content_type, url_override = False ):
        data = True
        i = 1
        results = []
        while data:
            if url_override:
                data = Importer.query_api( 'posts', url_override + '&order=desc&page=' + str(i))
            else:
                data = Importer.query_api(content_type + '?order=desc&page=' + str(i))
            if data: 
                for row in data:
                    results.append(row)
            i += 1
        return results

    def import_glossary_file( filename):

        path = settings.APPS_DIR
        file = "vandelay/import/glossary/" + filename + ".json"
        data = None

        with open( os.path.join(path,file)) as f:
            data = json.load(f)

        return data

    def import_filemaker_file( filename, listify = False ):
        
        path = settings.APPS_DIR
        file = "vandelay/import/" + filename + ".csv"
        
        # try to open the file
        try: 
            f = open( os.path.join( path, file ) )
        except FileNotFoundError:
            raise Exception("Couldn't open the following file: " + filename)
            return False

        if listify:
            results = []
            for row in f:
                row = Importer.clean_quoted_csv_row(row.rstrip().split('|'))
                results.append(row)
            return results
        else:
            return f

    def clean_quoted_csv_row( row_data ):
        cleaned = []
        for val in row_data:
            cleaned.append( val.strip().strip('"') )
        return cleaned

    def read_xml_import_file( filename ):

        # https://www.geeksforgeeks.org/xml-parsing-python/

        path = settings.APPS_DIR
        file = "vandelay/import/" + filename + ".xml"

        try: 
            f = open( os.path.join( path, file ) )
        except FileNotFoundError:
            raise Exception("Couldn't open the following file: " + filename)
            return False

        return f

    def parse_xml_import_file(filename):
        
        data = Importer.read_xml_import_file(filename)
        if not data: return False

        # create element tree object
        tree = ET.parse(data)

        # get root element 
        root = tree.getroot()

        # use the namespace from the root of the document
        ns = {
            'fm': 'http://www.filemaker.com/fmpxmlresult'
        }

        # get the header names
        header_items = []
        header = root.find('fm:METADATA',ns)
        for item in header.findall('fm:FIELD',ns):
            header_items.append(item.attrib['NAME'])
        
        items = []

        # get the resultset element
        resultset = root.find('fm:RESULTSET',ns)

        # loop through to get rows
        for item in resultset.findall('fm:ROW',ns):
            result = {}

            # get id from row attribute
            result['id'] = item.attrib['RECORDID']
            children = []

            # loop through to get columns
            for child in item.findall('fm:COL',ns):

                # get the column's single data element
                data_elem = child.find('fm:DATA',ns)
                if data_elem is not None:
                    the_text = data_elem.text.strip()
                else:
                    the_text = ''
                children.append(the_text)
            
            # zip together the header values and the row data
            for k, v in enumerate(header_items):
                result[v] = children[k]

            # save it
            items.append(result)

        results = {
            'header': header_items,
            'items': items
        }
        return results

    def write_xml_to_csv( data, filename ):

        path = settings.APPS_DIR
        file = "vandelay/import/master-" + filename + ".csv"
        
        # establish the header fields
        fields = ['id'] + data['header']

        print(data['items'])

        # writing to csv file
        with open( os.path.join(path,file), 'w' ) as csvfile:
            
            # creating a csv dict writer object 
            writer = csv.DictWriter(
                csvfile, 
                delimiter = '|',
                fieldnames = fields
            ) 

            # writing headers (field names) 
            writer.writeheader() 

            # writing data rows 
            writer.writerows(data['items']) 

        return csvfile

    def clean_up_spreadsheet_timestamp( cell_value, incoming_date_format="%Y-%m-%d" ):
        if cell_value == '': cell_value = datetime.datetime.now()
        else: cell_value = datetime.datetime.strptime(cell_value, incoming_date_format)
        return make_aware(cell_value)

    def clean_up_wordpress_timestamp(api_value):
        return make_aware(parse_datetime(api_value))

    


    # =================================
    # WP TAXONOMIES
    # =================================

    # an iterator, capable of calling query_api for all avaiable resources
    def query_taxonomy_api_all(resource):
        data = True
        i = 1
        results = []
        while data:
            data = Importer.query_api( 'taxonomy', resource + '?page=' + str(i))
            if data: 
                for row in data:
                    results.append(row)
            i += 1
        return results

    # basically a switch for handling different taxonomies
    def get_wp_taxonomy_details(argument):
        switcher = {
            'medium': {
                'path': 'ww-tax/mediums',
                'class': ArtworkMedium,
            },
            'aspect': {
                'path': 'ww-tax/aspects',
                'class': ArtworkAspect
            }
        }
        return switcher.get( argument, 'Invalid choice' )

    # import all videos by calling query_api_all, then add_update_wp_tax_object for each result
    def import_wp_taxonomy( tax_type ):
        taxonomy = Importer.get_wp_taxonomy_details(tax_type)
        api_records = Importer.query_taxonomy_api_all( api_root + taxonomy['path'] )
        if api_records:
            for a in api_records:
                Importer.add_update_wp_tax_object( a, taxonomy['class'] )
        if tax_type == 'medium': ImportLog.record('import_artwork_mediums')
        if tax_type == 'aspect': ImportLog.record('import_artwork_aspects')

    def add_update_wp_tax_object( api_blob, className ):

        data = api_blob

        # get the basics
        instance = {}
        instance['legacy_id'] = data['id']
        instance['title'] = data['name'].strip()
        instance['slug'] = data['slug'].strip()

        TaxObj = className

        # see whether a record with this legacy_id already exists
        try: existing = TaxObj.objects.get( legacy_id=instance['legacy_id'] )
        except: existing = False

        # if we have a match, update it
        if existing:
            existing.title = instance['title']
            existing.slug = instance['slug']
            existing.save()
            return existing

        # if we have no match, create a new record
        else:
            record = TaxObj(
                legacy_id = instance['legacy_id'],
                title = instance['title'],
                slug = instance['slug']
            )
            record.save()
            return record

    def connect_artworks_to_wp_tax( tax_type ):
        
        # get the details of the taxonomy
        taxonomy = Importer.get_wp_taxonomy_details(tax_type)

        # see if there are any Artwork records yet
        artwork_records = Artwork.objects.all()

        # if no artwork records yet, import them
        if not artwork_records:
            artwork_records = Importer.import_artworks()
        
        for a in artwork_records:
            # try to get a match in the media api
            attachment_data = Importer.query_api( 'media', api_root + 'media/' + str(a.legacy_id))
            if not attachment_data:
                raise Exception("Couldn't find a match for attachment with legacy id " + str(a.legacy_id) + "in media api")
            Importer.connect_artwork_to_wp_tax( a, attachment_data, tax_type )

        if tax_type == 'medium': ImportLog.record('connect_artwork_mediums')
        if tax_type == 'aspect': ImportLog.record('connect_artwork_aspects')

    def connect_artwork_to_wp_tax( artwork_record, attachment_blob, tax_type ):

        # set everything up
        artwork = artwork_record
        attachment = attachment_blob
        taxonomy = Importer.get_wp_taxonomy_details(tax_type)
        TaxClass = taxonomy['class']

        # see if we have any records for this taxonomy yet
        tax_records = TaxClass.objects.filter()

        # run the taxonomy import command, if we don't have any tax records yet
        if not tax_records:
            Importer.import_wp_taxonomy( tax_type )

        # get the tax ids from the attachent api record
        try: tax_data = attachment[taxonomy['path']]
        except: tax_data = []

        if tax_type is 'medium':

            # delete any existing mediums for this record
            if artwork.work_mediums.exists():
                artwork.work_mediums.clear()

            # search db for taxonomy records matching the ones the attachment api record has
            tax_results = TaxClass.objects.filter( legacy_id__in = tax_data )

            # if we got any results, save them to the artwork record
            if tax_results:
                artwork.work_mediums.set(tax_results)

        elif tax_type is 'aspect':
            
            # delete any existing mediums for this record
            artwork.work_aspects.clear()

            # search db for taxonomy records matching the ones the attachment api record has
            tax_results = TaxClass.objects.filter( legacy_id__in = tax_data )

            # if we got any results, save them to the artwork record
            if tax_results:
                artwork.work_aspects.set(tax_results)
        
        artwork.save()
        return artwork

    # =================================
    # IMAGES
    # =================================

    # import all videos by calling query_api_all, then add_update_image for each result
    def import_images( pages = 1 ):
        records = []
        if pages == 'all':
            records = Importer.query_api_all('media', api_root + 'media?media_type=image' )
        else:
            x = 1
            while x <= pages:
                data = Importer.query_api( 'media', api_root + 'media?media_type=image&order=desc&page=' + str(x))
                if data: 
                    for row in data:
                        records.append(row)
                x += 1
        if records:
            for i in records:
                Importer.add_update_image(i)
        ImportLog.record('import_images')

    def get_image_from_wp_api( id ):
        return Importer.query_api( 'media', api_root + 'media/' + str(id))

    # add or update a single Image (testable single function, runnable within larger loop)
    def add_update_image( api_blob = None ):

        if not api_blob: return None
        else: data = api_blob

        # get the basics
        legacy_id = int(data['id'])
        title = data['title']['rendered'].strip()
        slug = data['slug'].strip()
        alt = data['alt_text'].strip()
        caption = data['caption']['rendered'].strip()
        ww_img = data['source_url']

        # see whether a record with this legacy_id already exists
        try: existing = Image.objects.get( legacy_id=legacy_id )
        except: existing = False

        # if we have a match, update it
        if existing:
            existing.title = title
            existing.slug = slug
            existing.alt = alt
            existing.caption = caption
            existing.ww_img
            existing.save()
            return existing

        # if we have no match, create a new record
        else:
            record = Image(
                legacy_id = legacy_id,
                title = title,
                slug = slug,
                alt = alt,
                caption = caption,
                ww_img = ww_img
            )
            record.save()
            return record

    def get_scaled_image_dimensions( max_width, width, height ):

        if width > max_width:
            aspect_ratio = width / height
            new_height = max_width / aspect_ratio
            new_height = round(new_height)
            height = new_height
            width = max_width

        return (width,height)

    # convert an Image's ww_img attribute into a native, cropped image
    def add_update_image_file_from_own_url( image_id ):
        
        # see whether a record with this id already exists (bail if not)
        try: existing = Image.objects.get( legacy_id=int(image_id) )
        except ObjectDoesNotExist: 
            print("Couldn't find that record")
            return False

        # bail if we don't have an API img to work with
        if not existing.ww_img:
            print("No image to work with")
            return False

        # from: https://www.revsys.com/tidbits/loading-django-files-from-code

        # prepare and make the request for the image
        url = existing.ww_img
        filename = os.path.basename(urlparse(url).path)
        root_name, extension = os.path.splitext(filename)
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
        r = requests.get(url,headers=headers)

        # bail if we had a problem getting the image
        if not r or r.status_code != 200:
            print("Couldn't download the file")
            return False

        # https://www.shellvoide.com/python/compressing-images-and-managing-thumbnails-in-django-admin/
        
        # convert image bytes file to Pillow object
        img = Pillbug.open( BytesIO(r.content) )

        # calculcate new dimensions for scaling below a maximum width
        new_dimensions = Importer.get_scaled_image_dimensions(
            max_width = 2000,
            width = img.width,
            height = img.height
        )

        # deal with png images that come through with an rgba profile
        if img.mode == 'RGBA':
            img.convert('RGBA')
            background = Pillbug.new('RGBA',img.size, (255,255,255))
            alpha_composite = Pillbug.alpha_composite(background,img)
            img = alpha_composite.convert('RGB')
            filename = root_name + '.jpg'

        # optimize and resize the image
        img.convert('RGB')
        img.thumbnail(
            new_dimensions,
            Pillbug.ANTIALIAS
        )
        img.save( '/tmp/' + filename, format="JPEG", quality=100 )
        img.close()

        # write it to a temporary file
        '''with open("/tmp/" + filename, "wb") as f:
            print(f.width)
            f.write(r.content)'''        

        # reopen the temporary file
        reopen = open("/tmp/" + filename, "rb")
        django_file = File(reopen)

        # save the file to the model, and return the model
        existing.img.save( filename, django_file, save=True )
        return existing

    # =================================
    # VIDEOS
    # =================================

    # import all videos by calling query_api_all, then add_update_video for each result
    def import_videos():
        api_videos = Importer.query_api_all('videos')
        if api_videos:
            for v in api_videos:
                Importer.add_update_video(v)
        ImportLog.record('import_videos')

    # add or update a single video (testable single function, runnable within larger loop)
    def add_update_video( api_blob = None ):

        if not api_blob: return None
        else: data = api_blob
        
        # get the basics
        legacy_id = int(data['id'])
        title = data['title']['rendered'].strip()
        # description = data['content']['rendered'].strip() -- blank for now
        description = ''
        slug = data['slug']

        # format the original timestamp
        created_at = data['date_gmt']
        created_at = Importer.clean_up_wordpress_timestamp(created_at)

        # translate the post status
        if 'status' in data and data['status'] == 'publish': status = 'published'
        else: status = 'draft'

        # try to get the video url
        try: video_url = data['acf']['ww_video_embed']
        except: video_url = None

        # see whether a record with this legacy_id already exists
        try: existing = Video.objects.get( legacy_id=legacy_id )
        except: existing = False

        # if we have a match, update it
        if existing:
            existing.title = title
            existing.slug = slug
            existing.description = description
            existing.post_status = status
            existing.video_url = video_url
            existing.created_at = created_at
            existing.save()
            return existing

        # if we have no match, create a new record
        else:
            record = Video(
                legacy_id = legacy_id,
                title = title,
                description = description,
                slug = slug,
                post_status = status,
                video_url = video_url
            )
            record.save()
            record.created_at = created_at # override the default timestamp
            record.save()
            return record

    # =================================
    # POSTS
    # =================================

    # import all posts by calling query_api_all, then add_update_post for each result
    def import_posts():
        api_records = Importer.query_api_all('posts','http://blog.winstonwachter.com/wp-json/wp/v2/posts?categories=642')
        if api_records:
            for a in api_records:
                Importer.add_update_post(a)
        ImportLog.record('import_posts')

    # add or update a single post (testable single function, runnable within larger loop)
    def add_update_post( api_blob = None ):

        if not api_blob: return None
        else: data = api_blob
        
        # get the basics
        legacy_id = int(data['id'])
        title = data['title']['rendered'].strip()
        description = clean_html(data['content']['rendered'].strip())
        slug = data['slug']

        # format the original timestamp
        created_at = data['date_gmt']
        created_at = Importer.clean_up_wordpress_timestamp(created_at)

        # translate the post status
        if 'status' in data and data['status'] == 'publish': status = 'published'
        else: status = 'draft'

        # see whether a record with this legacy_id already exists
        try: existing = Post.objects.get( legacy_id=legacy_id )
        except: existing = False

        # if we have a match, update it
        if existing:
            existing.title = title
            existing.slug = slug
            existing.description = description
            existing.post_status = status
            existing.created_at = created_at
            existing.save()
            return existing

        # if we have no match, create a new record
        else:
            record = Post(
                legacy_id = legacy_id,
                title = title,
                description = description,
                slug = slug,
                post_status = status
            )
            record.save()
            record.created_at = created_at # override the original timestamp
            record.save()
            return record

    # =================================
    # ARTISTS
    # =================================

    def get_keys_for_master_inventory_file():
        return {
            'first_name': {
                'column_index': 1,
                'column_name': 'Artist First'
            },
            'last_name': {
                'column_index': 2,
                'column_name': 'Artist Last'
            },
            'legacy_id': {
                'column_index': 37,
                'column_name': 'artist-id'
            }
        }

    # import all artists by calling query_api_all, then add_update_artist for each result
    def import_artists():
        api_records = Importer.query_api_all('artists')
        if api_records:
            for a in api_records:
                Importer.add_update_artist(a)
        ImportLog.record('import_artists')

    # add or update a single video (testable single function, runnable within larger loop)
    def add_update_artist( api_blob = None ):

        if not api_blob: return None
        else: data = api_blob

        # get the basics
        legacy_id = int(data['id'])
        title = data['title']['rendered'].strip()
        slug = data['slug']
        description = clean_html(data['content']['rendered'].strip())

        # format the original timestamp
        created_at = data['date_gmt']
        created_at = Importer.clean_up_wordpress_timestamp(created_at)

        # translate the post status
        if 'status' in data and data['status'] == 'publish': status = 'published'
        else: status = 'draft'

        # figure out the artist's status at the gallery
        try: active_status = data['acf']['ww_artist_active_status']
        except: active_status = 'inactive'
        if active_status: active_status = active_status.strip()
        else: active_status = 'inactive'

        # try to get the artist's first name
        try: first_name = data['acf']['ww_artist_first_name']
        except: first_name = ''
        first_name = first_name.strip()

        # try to get the artist's first name
        try: last_name = data['acf']['ww_artist_last_name']
        except: last_name = ''
        last_name = last_name.strip()

        # try to get featured image id
        featured_image = None
        try: featured_image_id = data['featured_media']
        except: featured_image_id = None

        # if we got a featured image id, query the api for the image, 
        # and add/update the record in our database
        if featured_image_id:
            image_data_from_api = Importer.get_image_from_wp_api( featured_image_id )
            if image_data_from_api:
                featured_image = Importer.add_update_image( image_data_from_api )
                if featured_image: 
                    Importer.add_update_image_file_from_own_url( featured_image_id )

        # see whether a record with this legacy_id already exists
        try: existing = Artist.objects.get( legacy_id=legacy_id )
        except: existing = False

        # if we have a match, update it
        if existing:
            existing.title = title
            existing.slug = slug
            existing.description = description
            existing.post_status = status
            existing.active_status = active_status
            existing.available = True
            existing.first_name = first_name
            existing.last_name = last_name
            existing.created_at = created_at
            if featured_image: existing.featured_image = featured_image
            existing.save()
            return existing

        # if we have no match, create a new record
        else:
            record = Artist(
                legacy_id = legacy_id,
                title = title,
                description = description,
                slug = slug,
                post_status = status,
                active_status = active_status,
                available = True,
                first_name = first_name,
                last_name = last_name
            )
            record.save()
            record.created_at = created_at # override the default timestamp
            if featured_image: record.featured_image = featured_image
            record.save()
            return record

    def connect_artists_to_images( data = None, key = 'ww_artist_images' ):

        if not data:
            data = Importer.query_api_all('artists')
        
        if data:
            num_records = 0
            for row in data:
                try: artist = Importer.objects.get( legacy_id=row['id'] )
                except: artist = Importer.add_update_artist(row)
                Importer.connect_artist_to_images( artist, row, key )
                num_records = num_records + 1
        if key == 'ww_artist_installation_images': 
            ImportLog.record('connect_artist_install_images')
        else: 
            ImportLog.record('connect_artist_work_images')
        return num_records

    def connect_artist_to_images( artist_obj, artist_api_blob, key='ww_artist_images'  ):
    
        obj = artist_obj
        blob = artist_api_blob

        # see if the api record for this exhibition has any images
        try: images = blob['acf'][key]
        except: images = None

        # bail if no images (returning the original record)
        if not images or len(images) == 0: return obj

        # try to get/create records for each image in the api
        images_to_attach = []
        for image in images:
            
            # first see if we already have this image in the database
            try: existing = Image.objects.get( legacy_id=image['id'] )
            except: existing = False
            if existing: 
                images_to_attach.append(existing)
                continue
            
            # if we don't already have it, we need to go get it
            image_data_from_api = Importer.get_image_from_wp_api( image['id'] )
            if image_data_from_api:
                image_record = Importer.add_update_image( image_data_from_api )
                if image_record: images_to_attach.append(image_record)

        # delete any existing image associations for this Artwork
        if key == 'ww_artist_images':
            if obj.work_images.exists():
                obj.work_images.clear()
        elif key == 'ww_artist_installation_images':
            if obj.install_images.exists():
                obj.install_images.clear()

        # bail (and return original obj) if we don't have any images to attach at this point
        if not images_to_attach: return obj

        # otherwise let's attach each image to the record
        x = 1
        for i in images_to_attach:
            if key == 'ww_artist_installation_images':
                intermediary = ArtistInstallImage(
                    artist=obj,
                    image=i,
                    order=x
                )
            else:
                intermediary = ArtistImage(
                    artist=obj,
                    image=i,
                    order=x
                )
            intermediary.save()
            x = x + 1
        
        obj.save()
        return obj


    def add_update_artist_glossary():
        
        glossary_data = Importer.import_glossary_file('artists')

        inventory_data = Importer.import_filemaker_file( 'inventory' )

        for line in inventory_data:
            
            arr = line.rstrip().split('|')
            
            try: first_name = arr[0] + ' '
            except: first_name = ''
            
            try: last_name = arr[1]
            except: last_name = ''
            
            full_name = first_name + last_name
            
            if full_name not in glossary_data:
                glossary_data[full_name] = full_name

        print("LENGTH:")
        print(len(glossary_data))

        # sort the keys on glossary_data
        # create new empty dict sorted_glossary_data    
        # loop through the keys as key, val
            # sorted_glossary_data[key] = val
        # write sorted_glossary_data to file

        with open( os.path.join(settings.APPS_DIR,'vandelay/import/glossary/artists.json'), 'w' ) as f:
            # f.write(data)
            json.dump( glossary_data, f )

        return False

    def import_artists_from_spreadsheet( data = None ):

        ImportLog.record('import_artists_from_spreadsheet')

        if not data:
            data = Importer.import_filemaker_file( 'master-inventory', listify = True )
            header_row = data.pop(0)

        artists_imported = 0
        for row in data:
            data = Importer.import_artist_from_spreadsheet_row(row)
            if data:
                artists_imported = artists_imported + 1
        return artists_imported


    def import_artist_from_spreadsheet_row(row):

        # get the doc keys for this import
        keys = Importer.get_keys_for_master_inventory_file()

        # get our import values
        first_name = row[keys['first_name']['column_index']]
        last_name = row[keys['last_name']['column_index']]
        legacy_id = row[keys['legacy_id']['column_index']].strip()

        # bail if no legacy_id is present; we don't need this row for an artist import
        if legacy_id == '':
            return False
        
        # construct a title
        if first_name:
            title = first_name + ' ' + last_name
        else:
            title = last_name

        # construct a slug
        slug = slugify(title)

        # see whether a record with this legacy_id already exists
        try: existing = Artist.objects.get( legacy_id=legacy_id )
        except: existing = False

        # if we have a match, update only missing values
        if existing:
            if not existing.title:
                existing.title = title
            if not existing.first_name:
                existing.first_name = first_name
            if not existing.last_name:
                existing.last_name = last_name
            existing.available = False
            existing.save()
            return existing

        # if we have no match, create a new record
        else:
            record = Artist(
                legacy_id = legacy_id,
                title = title,
                slug = slug,
                first_name = first_name,
                last_name = last_name,
                available = False
            )
            record.save()
            return record

    def get_artist_from_wp_api( id ):
        return Importer.query_api( 'artist', api_root + 'artists/' + str(id))

    # =================================
    # EXHIBITIONS
    # =================================

    # import all exhibitions by calling query_api_all, then add_update_exhibition for each result
    def import_exhibitions():
        api_records = Importer.query_api_all('exhibitions')
        if api_records:
            for a in api_records:
                Importer.add_update_exhibition(a)
        ImportLog.record('import_exhibitions')

    # add or update a single exhibition (testable single function, runnable within larger loop)
    def add_update_exhibition( api_blob = None ):

        if not api_blob: return None
        else: data = api_blob

        # get the basics
        legacy_id = int(data['id'])
        title = data['title']['rendered'].strip()
        slug = data['slug']
        description = data['content']['rendered'].strip()

        # format the original timestamp
        created_at = data['date_gmt']
        created_at = Importer.clean_up_wordpress_timestamp(created_at)

        # translate the post status
        if 'status' in data and data['status'] == 'publish': status = 'published'
        else: status = 'draft'

        # translate the opening date (tricky bc of unpadded date)
        try: opening = data['acf']['ww_exh_opening']
        except: opening = None
        if opening:
            opening_date = parser.parse(opening)
            opening_date = opening_date.date()
        else:
            opening_date = None

        # translate the closing date (tricky bc of unpadded date)
        try: closing = data['acf']['ww_exh_closing']
        except: closing = None
        if closing:
            closing_date = parser.parse(closing)
            closing_date = closing_date.date()
        else:
            closing_date = None

        # get the string with the reception date information
        try: reception = data['acf']['ww_exh_opening_event_date']
        except: reception = ''
        if reception: reception = reception.strip()

        # figure out whether artist is in attendance
        try: artist_in_attendance = data['acf']['ww_exh_artist_attending']
        except: artist_in_attendance = None
        if artist_in_attendance and artist_in_attendance == 'yes':
            artist_in_attendance = True
        else:
            artist_in_attendance = False

        # try to get featured image id
        featured_image = None
        try: featured_image_id = data['featured_media']
        except: featured_image_id = None

        # if we got a featured image id, query the api for the image, 
        # and add/update the record in our database
        if featured_image_id:
            image_data_from_api = Importer.get_image_from_wp_api( featured_image_id )
            if image_data_from_api:
                featured_image = Importer.add_update_image( image_data_from_api )
                if featured_image: 
                    Importer.add_update_image_file_from_own_url( featured_image_id )

        # see whether a record with this legacy_id already exists
        try: 
            existing = Exhibition.objects.get( legacy_id=legacy_id )
            record = existing
        except: 
            existing = False

        # if we have a match, update it
        if existing:
            record.title = title
            record.description = description
            record.slug = slug
            record.post_status = status
            record.artist_in_attendance = artist_in_attendance
            record.open_date = opening_date
            record.close_date = closing_date
            record.reception = reception
            record.created_at = created_at
            if featured_image: record.featured_image = featured_image
            record.save()

        # if we have no match, create a new record
        else:
            record = Exhibition(
                legacy_id = legacy_id,
                title = title,
                description = description,
                slug = slug,
                post_status = status,
                open_date = opening_date,
                close_date = closing_date,
                artist_in_attendance = artist_in_attendance,
                reception = reception
            )
            record.save()
            if featured_image: record.featured_image = featured_image # only add featured image if we have one
            record.created_at = created_at # override the auto timestamp
            record.save()
        
        # splitting this out as separate administrative function now
        # record = Importer.connect_exhibition_to_images( record, data )
        record = Importer.connect_exhibition_to_artists( record, data )
        return record

    def connect_exhibitions_to_images( data = None ):

        if not data:
            data = Importer.query_api_all('exhibitions')
        
        if data:
            num_exhibitions = 0
            for row in data:
                try: exhibition = Importer.objects.get( legacy_id=row['id'] )
                except: exhibition = Importer.add_update_exhibition(row)
                Importer.connect_exhibition_to_images( exhibition, row )
        ImportLog.record('connect_exhibition_images')

    def connect_exhibition_to_images( exhibition_obj, exhibition_api_blob ):

        obj = exhibition_obj
        blob = exhibition_api_blob

        # see if the api record for this exhibition has any images
        try: images = blob['acf']['ww_exh_images']
        except: images = None

        # bail if no images (returning the original record)
        if not images or len(images) == 0: return obj

        # try to get/create records for each image in the api
        images_to_attach = []
        for image in images:
            
            # first see if we already have this image in the database
            try: existing = Image.objects.get( legacy_id=image['id'] )
            except: existing = False
            if existing: 
                images_to_attach.append(existing)
                continue
            
            # if we don't already have it, we need to go get it
            image_data_from_api = Importer.get_image_from_wp_api( image['id'] )
            if image_data_from_api:
                image_record = Importer.add_update_image( image_data_from_api )
                if image_record: images_to_attach.append(image_record)

        # delete any existing image associations for this Artwork
        if obj.images.exists():
            obj.images.clear()

        # bail (and return original obj) if we don't have any images to attach at this point
        if not images_to_attach: return obj

        # otherwise let's attach each image to the record
        x = 1
        for i in images_to_attach:
            intermediary = ExhibitionImage(
                exhibition=obj,
                image=i,
                order=x
            )
            intermediary.save()
            x = x + 1
        
        obj.save()
        return obj

    def connect_exhibition_to_artists( exhibition_obj, exhibition_api_blob ):

        obj = exhibition_obj
        blob = exhibition_api_blob

        # see if the api record for this exhibition has any images
        try: artists = blob['acf']['ww_exh_artists']
        except: artists = None

        # bail if no artists (returning the original record)
        if not artists or len(artists) == 0: return obj

        # try to get/create records for each image in the api
        artists_to_attach = []
        for artist in artists:
            
            # first see if we already have this image in the database
            try: existing = Artist.objects.get( legacy_id=artist['id'] )
            except: existing = False
            if existing: 
                artists_to_attach.append(existing)
                continue
            
            # if we don't already have it, we need to go get it
            artist_data_from_api = Importer.get_artist_from_wp_api( artist['ID'] )
            if artist_data_from_api:
                artist_record = Importer.add_update_artist( artist_data_from_api )
                if artist_record: artists_to_attach.append(artist_record)

        # delete any existing artist associations for this Exhibition
        if obj.artists.exists():
            obj.artists.clear()

        # bail (and return original obj) if we don't have any images to attach at this point
        if not artists_to_attach: return obj

        # otherwise let's attach each artist to the record
        for i in artists_to_attach:
            obj.artists.add(i)
        
        obj.save()
        return obj

    # =================================
    # ARTWORKS
    # =================================

    # an iterator, capable of calling query_api for all available resources
    def query_artworks_all():
        data = True
        i = 1
        results = []
        while data:
            data = Importer.query_api( 'artworks', 'https://seattle.winstonwachter.com/wp-json/ww-scour/v2/artwork?page=' + str(i)  )
            if data: 
                for row in data:
                    results.append(row)
            i += 1
        return results

    # add or update a single Artwork (testable single function, runnable within larger loop)
    def add_update_artwork( api_blob = None ):

        if not api_blob: return None
        else: data = api_blob
        
        # get the basics
        title = data['image_title'].strip()
        slug = data['image_slug'].strip()
        legacy_id = int(data['image_id'])
        status = 'published'

        # see if there are variations to grab
        try: api_variations = data['variations']
        except: api_variations = None

        # see if there's a filemaker id to grab
        try:
            fm_id = data['filemaker_id']
            if not fm_id:
                fm_id = ''
        except: fm_id = ''

        # try to get featured image id
        featured_image = None
        try: featured_image_id = data['featured_media']
        except: featured_image_id = None

        # see whether a record with this legacy_id already exists
        try: existing = Artwork.objects.get( legacy_id=legacy_id )
        except: existing = False

        # set up the record that we'll be returning
        record = None

        # if we have a match, update it
        if existing:
            existing.title = title
            existing.slug = slug
            existing.post_status = status
            existing.fm_id = fm_id
            existing.save()
            record = existing # this is now the record we'll eventually return

        # if we have no match, create a new record
        else:
            record = Artwork(
                legacy_id = legacy_id,
                title = title,
                slug = slug,
                post_status = status,
                fm_id = fm_id
            )
            record.save()

        # clear any existing variations for this record
        record.variations.all().delete()

        # if we found variations, let's deal with those now
        if api_variations:

            # now create records for each variation, attach them to the Artwork record
            for v in api_variations:
                variation = ArtworkVariation(
                    legacy_id = v['id'],
                    height = v['height'],
                    width = v['width'],
                    depth = v['depth'],
                    total_price = v['price'],
                    num_editions = v['num_editions'],
                    max_editions = v['max_editions'],
                    artwork = record
                )
                variation.save()

        return record

        # connect to mediums
        # Importer.connect_artwork_to_wp_taxonomy( record, 'medium' )

        # connect to aspects
        # Importer.connect_artwork_to_wp_taxonomy( record, 'aspect' )
        
        # connect to images
        # Importer.connect_artwork_to_images( record )

    # def connect_artwork_to_wp_taxonomy( artwork_record, tax_type )

    def import_artworks():
        api_records = Importer.query_artworks_all()
        if api_records:
            for a in api_records:
                Importer.add_update_artwork(a)
        ImportLog.record('import_artworks')

    def connect_artworks_to_images():

        # see if there are any Artwork records yet
        artwork_records = Artwork.objects.all()

        # if no artwork records yet, import them
        if not artwork_records:
            artwork_records = Importer.import_artworks()

        # see if there are any Image records yet
        image_records = Image.objects.all()

        # if no Image records yet, import them
        if not image_records:
            image_records = Importer.import_images()
        
        for a in artwork_records:
            # try to get a match in the media api
            attachment_data = Importer.query_api( 'media', api_root + 'media/' + str(a.legacy_id))
            if attachment_data:
                Importer.connect_artwork_to_images( a, attachment_data )
            # if not attachment_data:
                # raise Exception("Couldn't find a match for attachment with legacy id " + str(a.legacy_id) + "in the media api")      

        ImportLog.record('connect_artwork_images')

    def connect_artwork_to_images( artwork, attachment_data ):

        # create or update (and bring back) an Image record matching this attachment_data
        image_record = Importer.add_update_image( attachment_data )

        # if we have a bad Image record, just return with the Artwork record
        if not image_record:
            # print(artwork)
            # print("missing image record")
            return artwork

        # delete any existing image associations for this Artwork
        if artwork.work_images.exists():
                artwork.work_images.clear()

        intermediary = ArtworkImage(
            artwork=artwork,
            image=image_record,
            order=1
        )
        intermediary.save()
        
        artwork.save()
        return artwork

    def make_artwork_featured_image_from_work_images( artwork ):

        # bail if the artwork doesn't have any images to work with
        if not artwork.work_images.exists():
            print("Artwork with legacy_id " + str(artwork.legacy_id) + " has no images to work with")
            return False

        first_image = artwork.work_images.all()[0]
        artwork.featured_image = first_image
        artwork.save()
        return artwork

    def get_artwork_artists_from_artwork_titles():

        # see if there are any Artwork records yet
        artwork_records = Artwork.objects.all()

        # if no artwork records yet, import them
        if not artwork_records:
            artwork_records = Importer.import_artworks()

        # see if there are any Artist records yet
        artist_records = Artist.objects.all()

        # if no Artist records yet, import them
        if not artist_records:
            artist_records = Importer.import_artists()
        
        # loop through and run connector on each
        for a in artwork_records:
            Importer.get_artwork_artist_from_artwork_title(a)

        ImportLog.record('connect_artwork_artists')

    def get_artwork_artist_from_artwork_title(artwork):

        # split the artwork title on the pipe character
        parsed = artwork.title.split('|')

        # bail if the result has only one entry; means there was no pipe character
        if len(parsed) == 1:
            # print("No pipe present")
            return False

        # otherwise grab the name
        artist_name = parsed[0].strip()
        
        # try to get an artist with this exact name
        try: artist_record = Artist.objects.get( title=artist_name )
        except: artist_record = None

        # bail if we didn't find a matching artist
        if not artist_record:
            # print("Couldn't find that artist")
            return False

        # otherwise save the artist to the artwork and return the artwork
        artwork.artist = artist_record
        artwork.save()
        return artwork

    def call_houdini_api_for_all_artwork_images():

        # get artworks with related image records that have a ww_img attribute
        artworks = Artwork.objects.filter( work_images = True ).exclude( work_images__ww_img__isnull = True )
        for w in artworks:
            print(w)
            images = w.work_images.all()
            print(images)
            for i in images:
                print(i)
                if i.should_fetch_ar:
                    print("Should")
                else:
                    print("Shouldn't")
                i.should_fetch_ar = True
                i.save()
                if i.ar_ios:
                    print(i.ar_ios)
                else:
                    print("No ios AR")
                if i.ar_android:
                    print(i.ar_android)
                else:
                    print("No ios AR")

    def get_spreadsheet_keys_for_artwork_import():
        return {
            'legacy_id': {
                'column_index': 41,
                'column_name': 'wp-id'
            },
            'title': {
                'column_index': 27,
                'column_name': 'Title'
            },
            'year': {
                'column_index': 31,
                'column_name': 'Year'
            },
            'status': {
                'column_index': 26,
                'column_name': 'Status'
            },
            'fm_id': {
                'column_index': 16,
                'column_name': 'InventoryNumber'
            },
            'artist_id': {
                'column_index': 37,
                'column_name': 'artist-id'
            },
            'type_id': {
                'column_index': 38,
                'column_name': 'type-id'
            },
            'bought_date': {
                'column_index': 4,
                'column_name': 'Bought Date'
            },
            'bought_from': {
                'column_index': 5,
                'column_name': 'Bought From'
            },
            'bought_price': {
                'column_index': 6,
                'column_name': 'Bought Price'
            },
            'bought_by': {
                'column_index': 25,
                'column_name': 'Sold To'
            },
            'invoice_num': {
                'column_index': 17,
                'column_name': 'Invoice'
            },
            'removed_date': {
                'column_index': 23,
                'column_name': 'RemovedDate'
            },
            'buyer_name': {
                'column_index': 25,
                'column_name': 'Sold To'
            },
            'buyer_id': {
                'column_index': 40,
                'column_name': 'sold-to-contact-id'
            }
        }

    def add_update_artworks_from_spreadsheet( data = None, test = False ):

        if not test:
            ImportLog.record('import_artwork_from_spreadsheet')

        if not data:
            data = Importer.import_filemaker_file( 'master-inventory', listify = True )
            header_row = data.pop(0)

        records_imported = 0
        for row in data:
            data = Importer.add_update_artwork_from_spreadsheet_row(row)
            if data:
                records_imported = records_imported + 1
        print("Records imported: " + str(records_imported))
        return records_imported

    def add_update_artwork_from_spreadsheet_row( row ):
        
        # get the doc keys for this import
        keys = Importer.get_spreadsheet_keys_for_artwork_import()

        # get our import values
        legacy_id = row[keys['legacy_id']['column_index']].strip()
        title = row[keys['title']['column_index']].strip()
        year = row[keys['year']['column_index']].strip()
        status = row[keys['status']['column_index']].strip()
        fm_id = row[keys['fm_id']['column_index']].strip()
        artist_id = row[keys['artist_id']['column_index']].strip()
        type_id = row[keys['type_id']['column_index']].strip()

        # bail if no FileMaker id; we're considering that the legacy id in this process
        if fm_id == "":
            print("Missing FileMaker id")
            return None

        # bail if status is Sold (not importing these ones)
        '''if status == "Sold":
            print("Skipping sold")
            return None

        # bail if status is Removed (not importing these ones, for now)
        if status == "Removed":
            print("Skipping removed")
            return None'''

        # try to get an Artist object from the row value
        related_artist = None
        if artist_id != '':
            try: related_artist = Artist.objects.get( legacy_id=artist_id )        
            except: related_artist = None

        # try to get an ArtworkType object from the row value
        artwork_type = None
        if type_id != '':
            try: artwork_type = ArtworkType.objects.get( legacy_id=type_id )
            except: artwork_type = None

        # see if we already have a record of this Artwork
        try: 
            existing = Artwork.objects.get( fm_id=fm_id )
            record = existing
        except: 
            existing = False

        # if we already have a record, update it
        if existing:
            if not record.year: record.year = year
            if related_artist and not record.artist: record.artist = related_artist
            if record.work_types.exists(): record.work_types.clear()
            if artwork_type: record.work_types.add(artwork_type)
            # if legacy_id: record.legacy_id = legacy_id
            if record.searchable:
                record.searchable = True
            else:
                record.searchable = False
            record.save()

        # else if we don't have a record, create one
        else:
            record = Artwork(
                title = title,
                # legacy_id = legacy_id,
                fm_id = fm_id,
                year = year,
                searchable = False
            )
            record.save()
            if related_artist: record.artist = related_artist # add artist after save bc it's conditional
            if artwork_type: record.work_types.add(artwork_type) # art ArtworkType relationships after save bc they're conditional
            record.save()
        
        # delete any existing history for this Artwork record
        if record.history.exists():
            record.history.all().delete()
        
        # add a Bought From note to this record, if relevant
        Importer.maybe_add_update_artwork_bought_note( record, row, keys )
        Importer.maybe_add_update_artwork_invoice_note( record, row, keys )
        Importer.maybe_add_update_artwork_removed_note( record, row, keys )
        Importer.maybe_add_update_artwork_sold_note( record, row, keys )
        
        return record

    def maybe_add_update_artwork_bought_note( artwork, spreadsheet_row, keys = None ):
        
        if not keys:
            # get the doc keys for this import
            keys = Importer.get_spreadsheet_keys_for_artwork_import()

        # get our import values
        row = spreadsheet_row
        bought_date = row[keys['bought_date']['column_index']].strip()
        bought_from = row[keys['bought_from']['column_index']].strip()
        bought_price = row[keys['bought_price']['column_index']].strip()
        created_at = row[keys['bought_price']['column_index']].strip()

        message = ''

        # bail if we don't have a bought date; that's the bare minimum we need
        if bought_date == '':
            return None

        # format the date to use as a timestamp later
        timestamp = Importer.clean_up_spreadsheet_timestamp(bought_date)

        message = message + 'Bought '

        if bought_from != '':
            message = message + 'from ' + bought_from + ' '

        if bought_price != '':
            if bought_price[0:1] == '$': bought_price.lstrip('$')
            message = message + 'for $' + bought_price + ' '

        message = message + 'on ' + bought_date + '.'

        note = Moment(
            description = message,
            artwork = artwork
        )
        note.save()
        note.created_at = timestamp
        note.save()
        return note

    def maybe_add_update_artwork_invoice_note( artwork, spreadsheet_row, keys = None ):
        
        if not keys:
            # get the doc keys for this import
            keys = Importer.get_spreadsheet_keys_for_artwork_import()

        # get our import values
        row = spreadsheet_row
        invoice_num = row[keys['invoice_num']['column_index']].strip()

        # bail if we don't have what we need
        if invoice_num == '':
            return ''

        # change_url = reverse( 'admin:docent_artwork_change', args=(artwork.id,) )

        message = 'FileMaker Invoice No. ' + invoice_num

        note = Moment(
            description = message,
            artwork = artwork
        )
        note.save()
        return note

    def maybe_add_update_artwork_removed_note( artwork, spreadsheet_row, keys = None ):
        
        if not keys:
            # get the doc keys for this import
            keys = Importer.get_spreadsheet_keys_for_artwork_import()

        # get our import values
        row = spreadsheet_row
        removed_date = row[keys['removed_date']['column_index']].strip()

        # bail if we don't have what we need
        if removed_date == '':
            return ''
        
        # print(artwork.title + " (" + str(artwork.id) + ")")

        # format the date to use as a timestamp later
        timestamp = Importer.clean_up_spreadsheet_timestamp(removed_date)

        message = 'Removed from inventory ' + removed_date

        note = Moment(
            description = message,
            artwork = artwork
        )
        note.save()
        note.created_at = timestamp
        note.save()
        return note

    def maybe_add_update_artwork_sold_note( artwork, spreadsheet_row, keys = None ):
        
        if not keys:
            # get the doc keys for this import
            keys = Importer.get_spreadsheet_keys_for_artwork_import()

        # get our import values
        row = spreadsheet_row
        buyer_id = row[keys['buyer_id']['column_index']].strip()
        buyer_name = row[keys['buyer_name']['column_index']].strip()
        status = row[keys['status']['column_index']].strip()

        # bail if this isn't marked as Sold
        if status != 'Sold': 
            return ''

        # also bail if we don't have the buyer name (no sense just noting that it was sold)
        if buyer_name == '':
            return ''

        # print(artwork.title + " (" + str(artwork.id) + ")")

        # assemble the return string
        message = "Sold to " + buyer_name 

        # create the Moment record
        note = Moment(
            description = message,
            artwork = artwork
        )
        note.save()

        # see if there's an active Contact record we can tie to the note
        if buyer_id != '':
            try: contact_record = Contact.objects.get( legacy_id=buyer_id )
            except: contact_record = None
            if contact_record:
                note.contact = contact_record
                note.save()
        
        return note

    def import_artwork_types_from_spreadsheet( data = None ):

        ImportLog.record('import_artwork_types')

        # if the data wasn't passed in, create it
        if not data:
            data = Importer.import_filemaker_file( 'inventory-types', listify = True )
            header_row = data.pop(0)

        record_count = 0

        for row in data:

            id = row[0]
            title = row[1]

            try: existing = ArtworkType.objects.get( legacy_id=id )
            except: existing = None

            if existing:
                existing.title = title
                existing.save()
                record_count = record_count + 1
                # print("Updated record " + str(id))

            else:
                record = ArtworkType(
                    legacy_id = id,
                    title = title,
                    slug = slugify(title)
                )
                record.save()
                record_count = record_count + 1
                # print("Created record " + str(id))

        return record_count

    def import_artwork_locations_from_spreadsheet( data = None ):

        ImportLog.record('import_artwork_locations')

        # if the data wasn't passed in, create it
        if not data:
            data = Importer.import_filemaker_file( 'inventory-locations', listify = True )
            header_row = data.pop(0)

        record_count = 0

        for row in data:

            id = row[0]
            title = row[1].strip()

            try: existing = ArtworkLocation.objects.get( legacy_id=id )
            except: existing = None

            if existing:
                existing.title = title
                existing.save()
                record_count = record_count + 1
                print("Updated location " + str(id))

            else:
                record = ArtworkLocation(
                    legacy_id = id,
                    title = title,
                    slug = slugify(title)
                )
                record.save()
                record_count = record_count + 1
                print("Created location " + str(id))

        return record_count
    
    # =================================
    # CONTACTS
    # =================================

    def create_update_contact_categories_from_spreadsheet():

        ImportLog.record('import_contact_categories')

        source_data = Importer.import_filemaker_file( 'contact-categories', listify = True )
        created_entries = []
        
        if not source_data:
            return None
        
        for row in source_data:

            legacy_id = row[0]
            title = row[1]
            slug = slugify(title)

            # check for existing
            try: existing = ContactCategory.objects.get( legacy_id = legacy_id )
            except: existing = False

            # if one already exists, update it
            if existing:
                existing.title = title
                existing.save()
                created_entries.append(existing)

            # else if we have no match, create a new record
            else:
                record = ContactCategory(
                    legacy_id = legacy_id,
                    title = title,
                    slug = slug
                )
                record.save()
                created_entries.append(record)

        return created_entries

    def get_spreadsheet_keys_for_contact_import():
        return {
            'legacy_id': {
                'column_index': 0,
                'column_name': 'id'
            },
            'first_name': {
                'column_index': 4,
                'column_name': 'First name'
            },
            'last_name': {
                'column_index': 5,
                'column_name': 'Last name'
            },
            'position': {
                'column_index': 7,
                'column_name': 'Title'
            },
            'salutation': {
                'column_index': 3,
                'column_name': 'Salutation'
            },
            'company': {
                'column_index': 6,
                'column_name': 'Company'
            },
            'admin_notes': {
                'column_index': 20,
                'column_name': 'Notes'
            },
            'created_at': {
                'column_index': 1,
                'column_name': 'Date added'
            }
        }

    def import_update_contact_basics_from_spreadsheet( data = None, test = False ):

        if not test:
            ImportLog.record('import_contacts')

        if not data:
            data = Importer.import_filemaker_file( 'master-contacts', listify = True )
            header_row = data.pop(0)

        records_imported = 0
        for row in data:
            data = Importer.import_update_contact_basics_from_spreadsheet_row(row)
            if data:
                records_imported = records_imported + 1
        return records_imported

    def import_update_contact_basics_from_spreadsheet_row( row ):
        
        # get the doc keys for this import
        keys = Importer.get_spreadsheet_keys_for_contact_import()

        # get our import values
        legacy_id = row[keys['legacy_id']['column_index']].strip()
        first_name = row[keys['first_name']['column_index']].strip()
        last_name = row[keys['last_name']['column_index']].strip()
        position = row[keys['position']['column_index']].strip()
        salutation = row[keys['salutation']['column_index']].strip()
        company = row[keys['company']['column_index']].strip()
        admin_notes = row[keys['admin_notes']['column_index']].strip()
        
        # convert the given time into a timestamp, set its timezone to the one used by the site
        created_at = row[keys['created_at']['column_index']].strip()
        if created_at == '':
            created_at = datetime.datetime.now()
            created_at = make_aware(created_at)
        else:
            created_at = datetime.datetime.strptime(created_at, '%Y-%m-%d')
            created_at = make_aware(created_at)

        # see whether a record with this legacy_id already exists
        try: existing = Contact.objects.get( legacy_id=legacy_id )
        except: existing = False

        # if we have a match, update only missing values
        if existing:
            if not existing.first_name: existing.first_name = first_name
            if not existing.last_name: existing.last_name = last_name
            if not existing.position: existing.position = position
            if not existing.salutation: existing.salutation = salutation
            if not existing.company: existing.company = company
            if not existing.admin_notes: existing.admin_notes = admin_notes
            if not existing.created_at: existing.created_at = created_at
            existing.save()
            return existing

        # if we have no match, create a new record
        else:
            record = Contact(
                legacy_id = legacy_id,
                first_name = first_name,
                last_name = last_name,
                position = position,
                salutation = salutation,
                company = company,
                admin_notes = admin_notes
            )
            record.save()
            record.created_at = created_at # override timestamp after save
            record.save() # and then save again
            return record

    def get_spreadsheet_keys_for_contact_artists():
        return {
            'legacy_id': {
                'column_index': 0,
                'column_name': 'id'
            },
            'artists': {
                'column_index': 24,
                'column_name': 'artist-id'
            },
            'created_at': {
                'column_index': 1,
                'column_name': 'Date added'
            }
        }

    def import_contact_artist_preferences_from_spreadsheet( data = None ):

        ImportLog.record('connect_contact_artists')

        # get spreadsheet data if it wasn't passed in
        if not data:
            data = Importer.import_filemaker_file( 'master-contacts', listify = True )
            header_row = data.pop(0)

        # do a fresh import/update of artist info, if needed
        artists_exist = Artist.objects.count()
        if not artists_exist:
            print("Better import some artists")
            Importer.import_artists_from_spreadsheet()

        # now loop through the data and create the relationships
        relationships_created = 0
        for row in data:
            contact = Importer.import_contact_artist_preference_from_spreadsheet_row(row)
            if contact:
                relationships_created = relationships_created + len(contact.artists.all())
        return relationships_created

    def import_contact_artist_preference_from_spreadsheet_row(row):

        # get the doc keys for this import
        keys = Importer.get_spreadsheet_keys_for_contact_artists()

        # get our import values
        legacy_id = row[keys['legacy_id']['column_index']].strip()
        artists = row[keys['artists']['column_index']].strip()

        # bail if we don't have the data we need
        if not legacy_id or artists == '':
            # print("Missing legacy id or artist information")
            return None

        # split up the value of the address cell, as it may be comma-delimited
        artists = artists.split('/')

        # convert the given time into a timestamp, set its timezone to the one used by the site
        created_at = row[keys['created_at']['column_index']].strip()
        if created_at == '':
            created_at = datetime.datetime.now()
            created_at = make_aware(created_at)
        else:
            created_at = datetime.datetime.strptime(created_at, '%Y-%m-%d')
            created_at = make_aware(created_at)

        # get the Contact object associated with this row
        try: existing_contact = Contact.objects.get( legacy_id=legacy_id )
        except: existing_contact = Importer.import_update_contact_basics_from_spreadsheet_row(row)

        # bail if we still don't have a contact
        if not existing_contact:
            # print("No contact to attach these artists to (" + legacy_id + ")")
            return None

        # loop through the passed artist ids, get objects
        found_artists = []
        for id in artists:
            try: artist = Artist.objects.get( legacy_id = id )
            except: artist = None
            if artist: 
                # print( str(existing_contact.legacy_id) + " likes " + artist.last_name)
                found_artists.append(artist)

        # clear any of the contact's existing artist preferences
        if existing_contact.artists.all().exists():
            existing_contact.artists.clear()

        # add the new ones
        if found_artists:
            existing_contact.artists.set(found_artists)

        # save and return the record
        existing_contact.save()
        return existing_contact

    def get_spreadsheet_keys_for_contact_categories():
        return {
            'legacy_id': {
                'column_index': 0,
                'column_name': 'id'
            },
            'categories': {
                'column_index': 23,
                'column_name': 'category-id'
            },
            'created_at': {
                'column_index': 1,
                'column_name': 'Date added'
            }
        }

    def import_contact_categories_from_spreadsheet( data = None):

        ImportLog.record('connect_contact_categories')

        # get spreadsheet data if it wasn't passed in
        if not data:
            data = Importer.import_filemaker_file( 'master-contacts', listify = True )
            header_row = data.pop(0)

        # do a fresh import/update of artist info, if needed
        categories_exist = ContactCategory.objects.count()
        if not categories_exist:
            print("Better import some categories")
            Importer.create_update_contact_categories_from_spreadsheet()

        # now loop through the data and create the relationships
        contacts_modified = 0
        for row in data:
            contact = Importer.import_contact_category_from_spreadsheet_row(row)
            if contact:
                contacts_modified = contacts_modified + 1
        return contacts_modified

    def import_contact_category_from_spreadsheet_row(row):
        
        # get the doc keys for this import
        keys = Importer.get_spreadsheet_keys_for_contact_categories()

        # get our import values
        legacy_id = row[keys['legacy_id']['column_index']].strip()
        categories = row[keys['categories']['column_index']].strip()

        # bail if we don't have the data we need
        if not legacy_id or categories == '':
            print("Missing legacy id or category information")
            return None

        # split up the value of the address cell, as it may be comma-delimited
        categories = categories.split('/')

        # convert the given time into a timestamp, set its timezone to the one used by the site
        created_at = row[keys['created_at']['column_index']].strip()
        if created_at == '':
            created_at = datetime.datetime.now()
            created_at = make_aware(created_at)
        else:
            created_at = datetime.datetime.strptime(created_at, '%Y-%m-%d')
            created_at = make_aware(created_at)

        # get the Contact object associated with this row
        try: existing_contact = Contact.objects.get( legacy_id=legacy_id )
        except: existing_contact = Importer.import_update_contact_basics_from_spreadsheet_row(row)

        # bail if we still don't have a contact
        if not existing_contact:
            print("No contact to attach these artists to (" + legacy_id + ")")
            return None

        # loop through the passed artist ids, get objects
        found_categories = []
        for id in categories:
            try: category = ContactCategory.objects.get( legacy_id = id )
            except: category = None
            if category: 
                print( str(existing_contact.legacy_id) + " is of type " + category.title)
                found_categories.append(category)

        # clear any of the contact's existing artist preferences
        if existing_contact.categories.all().exists():
            existing_contact.categories.clear()

        # add the new ones
        if found_categories:
            existing_contact.categories.set(found_categories)

        # save and return the record
        existing_contact.save()
        return existing_contact

    # =====================
    # ADDRESSES
    # =====================

    def get_spreadsheet_keys_for_address_import():
        return {
            'legacy_id': {
                'column_index': 0,
                'column_name': 'id'
            },
            'address_1': {
                'column_index': 8,
                'column_name': 'Address1'
            },
            'address_2': {
                'column_index': 9,
                'column_name': 'Address2'
            },
            'city': {
                'column_index': 10,
                'column_name': 'City'
            },
            'state': {
                'column_index': 11,
                'column_name': 'State'
            },
            'zipcode': {
                'column_index': 12,
                'column_name': 'Zip'
            },
            'country': {
                'column_index': 13,
                'column_name': 'Country'
            },
            'created_at': {
                'column_index': 1,
                'column_name': 'Date added'
            }
        }

    def import_update_addresses_from_spreadsheet( data = None):

        ImportLog.record('import_contact_addresses')

        if not data:
            data = Importer.import_filemaker_file( 'master-contacts', listify = True )
            header_row = data.pop(0)

        records_imported = 0
        for row in data:
            data = Importer.import_update_address_from_spreadsheet_row(row)
            if data:
                records_imported = records_imported + 1
        return records_imported

    def import_update_address_from_spreadsheet_row( row ):
        
        # get the doc keys for this import
        keys = Importer.get_spreadsheet_keys_for_address_import()

        # get our import values
        legacy_id = row[keys['legacy_id']['column_index']].strip()
        address_1 = row[keys['address_1']['column_index']].strip()
        address_2 = row[keys['address_2']['column_index']].strip()
        city = row[keys['city']['column_index']].strip()
        state = row[keys['state']['column_index']].strip()
        zipcode = row[keys['zipcode']['column_index']].strip()
        country = row[keys['country']['column_index']].strip()

        # bail if we don't have a row number
        if not legacy_id:
            return None

        # bail if we're missing all of the relevant address fields
        if address_1 == '' and address_2 == '' and city == '' and state == '' and zipcode == '' and country == '':
            return None        
        
        # convert the given time into a timestamp, set its timezone to the one used by the site
        created_at = row[keys['created_at']['column_index']].strip()
        if created_at == '':
            created_at = datetime.datetime.now()
            created_at = make_aware(created_at)
        else:
            created_at = datetime.datetime.strptime(created_at, '%Y-%m-%d')
            created_at = make_aware(created_at)

        # see whether a record with this legacy_id already exists
        try: existing = Address.objects.get( legacy_id=legacy_id )
        except: existing = False

        # get the Contact object associated with this row
        try: 
            existing_contact = Contact.objects.get( legacy_id=legacy_id )
        except: 
            existing_contact = Importer.import_update_contact_basics_from_spreadsheet_row(row)

        # bail if we still don't have a contact
        if not existing_contact:
            print("No contact to attach this address to (" + legacy_id + ")")
            return None

        # if we have an Address match, update the values
        if existing: 

            # clear any of the contact's any existing addresses
            existing.contact = existing_contact
            if existing.contact.addresses.all().exists():
                existing.contact.addresses.clear()

            # update the Address record
            existing.address_1 = address_1
            existing.address_2 = address_2
            existing.city = city
            existing.state = state
            existing.zipcode = zipcode
            existing.country = country
            existing.save()
            return existing

        # if we have no match, create a new record
        else:
            record = Address(
                legacy_id = legacy_id,
                address_1 = address_1,
                address_2 = address_2,
                city = city,
                state = state,
                zipcode = zipcode,
                country = country,
                contact = existing_contact,
                primary = True
            )
            record.save()
            record.created_at = created_at # override timestamp after save
            # if existing_contact: record.contact = existing_contact # save the contact fk
            record.save() # and then save again
            return record

    # ============================
    # TELEPHONES
    # ============================
    def get_spreadsheet_keys_for_telephone_import():
        return {
            'legacy_id': {
                'column_index': 0,
                'column_name': 'id'
            },
            'work_number': {
                'column_index': 25,
                'column_name': 'work-phone-cleaned'
            },
            'work_number_note': {
                'column_index': 26,
                'column_name': 'work-phone-note'
            },
            'home_number': {
                'column_index': 27,
                'column_name': 'home-phone-cleaned'
            },
            'home_number_note': {
                'column_index': 28,
                'column_name': 'home-phone-note'
            },
            'other_number': {
                'column_index': 29,
                'column_name': 'other-phone-cleaned'
            },
            'other_number_note': {
                'column_index': 30,
                'column_name': 'other-phone-note'
            },
            'created_at': {
                'column_index': 1,
                'column_name': 'Date added'
            }
        }

    def import_telephones_from_spreadsheet( data = None, phone_type = None ):

        ImportLog.record('import_contact_phones')

        # create data if it wasn't passed in
        if not data:
            data = Importer.import_filemaker_file( 'master-contacts', listify = True )
            header_row = data.pop(0)

        # delete all existing Telephone records
        Telephone.objects.all().delete()

        records_imported = 0

        if phone_type:
            for row in data:
                results = Importer.import_telephone_from_spreadsheet_row( row, phone_type )
                if results:
                    records_imported = records_imported + 1
        else:
            print("Mass!")
            for row in data:
                for phone_type in ['work','home','other']:
                    print("Starting phone type " + phone_type)
                    results = Importer.import_telephone_from_spreadsheet_row( row, phone_type )
                    if results:
                        records_imported = records_imported + 1
        
        return records_imported

    def import_telephone_from_spreadsheet_row( row, number_type = 'work' ):

        # NOTE: This only adds records. It doesn't modify existing ones.
        # So it assumes that all records of this type have been deleted first.

        # get the doc keys for this import
        keys = Importer.get_spreadsheet_keys_for_telephone_import()

        # set the key we're looking for, using the type variable
        type_key = number_type + "_number"
        note_key = number_type + "_number_note"

        # get our import values
        legacy_id = row[keys['legacy_id']['column_index']].strip()
        number = row[keys[type_key]['column_index']].strip()
        note = row[keys[note_key]['column_index']].strip()

        # bail if we don't have the data we need
        if not legacy_id or number == '':
            print("Missing something")
            return None

        # convert the given time into a timestamp, set its timezone to the one used by the site
        created_at = row[keys['created_at']['column_index']].strip()
        if created_at == '':
            created_at = datetime.datetime.now()
            created_at = make_aware(created_at)
        else:
            created_at = datetime.datetime.strptime(created_at, '%Y-%m-%d')
            created_at = make_aware(created_at)

        # get the Contact object associated with this row
        try: existing_contact = Contact.objects.get( legacy_id=legacy_id )
        except: existing_contact = Importer.import_update_contact_basics_from_spreadsheet_row(row)

        # bail if we still don't have a contact
        if not existing_contact:
            print("No contact to attach this phone number to (" + legacy_id + ")")
            return None

        record = Telephone(
            number = number,
            type = number_type,
            note = note,
            contact = existing_contact
        )
        
        record.save()
        record.created_at = created_at # override timestamp after save
        if number_type == 'work': record.primary = True # save Primary? status after save
        record.save() # and then save again
        return record

    # ============================
    # EMAILS
    # ============================
    
    def get_spreadsheet_keys_for_email_import():
        return {
            'legacy_id': {
                'column_index': 0,
                'column_name': 'id'
            },
            'address': {
                'column_index': 17,
                'column_name': 'email'
            },
            'created_at': {
                'column_index': 1,
                'column_name': 'Date added'
            }
        }

    def import_emails_from_spreadsheet( data = None ):

        ImportLog.record('import_contact_emails')

        # create data if it wasn't passed in
        if not data:
            data = Importer.import_filemaker_file( 'master-contacts', listify = True )
            header_row = data.pop(0)

        # delete all existing Telephone records
        Email.objects.all().delete()

        records_imported = 0
        
        for row in data:    
            results = Importer.import_email_from_spreadsheet_row( row )
            if results:
                records_imported = records_imported + len(results)
        
        return records_imported

    def import_email_from_spreadsheet_row( row ):

        # NOTE: This only adds records. It doesn't modify existing ones.
        # So it assumes that all records of this type have been deleted first.

        # get the doc keys for this import
        keys = Importer.get_spreadsheet_keys_for_email_import()

        # get our import values
        legacy_id = row[keys['legacy_id']['column_index']].strip()
        address = row[keys['address']['column_index']].strip()

        # bail if we don't have the data we need
        if not legacy_id or address == '':
            print("Missing something")
            return None

        # split up the value of the address cell, as it may be comma-delimited
        addresses = address.split(',')

        # convert the given time into a timestamp, set its timezone to the one used by the site
        created_at = row[keys['created_at']['column_index']].strip()
        if created_at == '':
            created_at = datetime.datetime.now()
            created_at = make_aware(created_at)
        else:
            created_at = datetime.datetime.strptime(created_at, '%Y-%m-%d')
            created_at = make_aware(created_at)

        # get the Contact object associated with this row
        try: existing_contact = Contact.objects.get( legacy_id=legacy_id )
        except: existing_contact = Importer.import_update_contact_basics_from_spreadsheet_row(row)

        # bail if we still don't have a contact
        if not existing_contact:
            print("No contact to attach this email to (" + legacy_id + ")")
            return None

        records_created = []
        for address in addresses:
            
            record = Email(
                address = address,
                contact = existing_contact
            )
            
            record.save()
            record.created_at = created_at # override timestamp after save
            record.save() # and then save again

            records_created.append(record)

        return records_created

    # ================
    # CONSIGNMENTS
    # ================
    
    def get_spreadsheet_keys_for_consignment_import():
        return {
            'date': {
                'column_index': 0,
                'column_name': 'Agmt Date'
            },
            'legacy_id': {
                'column_index': 1,
                'column_name': 'Agmt Number'
            },
            'commission': {
                'column_index': 2,
                'column_name': 'Commission'
            },
            'client_name': {
                'column_index': 3,
                'column_name': 'Consigned to'
            },
            'client_id': {
                'column_index': 4,
                'column_name': 'consigned-to-id'
            },
            'admin_notes': {
                'column_index': 5,
                'column_name': 'Contract Notes'
            },
            'total_value': {
                'column_index': 7,
                'column_name': 'Total Asking'
            },
            'item_count': {
                'column_index': 6,
                'column_name': 'ItemCount'
            },
            'artwork_id': {
                'column_index': 15,
                'column_name': 'SE_Consignment Items::Inventory 1'
            },
            'line_item_value': {
                'column_index': 12,
                'column_name': 'SE_Consignment Items::AskingPriceCalc'
            }
        }

    def add_update_consignments( data = None, test = False ):

        if not test:
            ImportLog.record('import_consignments')

        if not data:
            data = Importer.import_filemaker_file( 'master-consignments', listify = True )
            header_row = data.pop(0)

        # delete all consignment line items before proceeding
        ConsignmentWork.objects.all().delete()

        records_imported = 0
        for row in data:
            data = Importer.add_update_consignment(row)
            if data:
                records_imported = records_imported + 1
        return records_imported

    def add_update_consignment(row):

        # get the doc keys for this import
        keys = Importer.get_spreadsheet_keys_for_consignment_import()

        # get our import values
        legacy_id = row[keys['legacy_id']['column_index']].strip()
        title = row[keys['client_name']['column_index']].strip()
        date = row[keys['date']['column_index']].strip()
        commission = row[keys['commission']['column_index']].strip()
        admin_notes = row[keys['admin_notes']['column_index']].strip()
        item_count = row[keys['item_count']['column_index']].strip()
        total_value = row[keys['total_value']['column_index']].strip()
        client_id = row[keys['client_id']['column_index']].strip()
        artwork_id = row[keys['artwork_id']['column_index']].strip()
        line_item_value = row[keys['line_item_value']['column_index']].strip()
        client_name = title

        print(legacy_id)

        # bail if no legacy id or artwork id
        if legacy_id == '' or artwork_id == '':
            return None

        # make the total_value 0 if not otherwise set
        if total_value == '':
            total_value = 0

        # format the date as a Django timestamp
        if date != '':
            timestamp = Importer.clean_up_spreadsheet_timestamp( date, "%m/%d/%Y" )
        else:
            timestamp = datetime.datetime.now()

        # look for an existing Contact record
        client = None
        if client_id != '':
            try: client = Contact.objects.get( legacy_id=client_id )
            except: client = None
        # look for an existing Artwork record
        try: 
            existing = Consignment.objects.get( legacy_id=legacy_id )
            record = existing
        except: 
            existing = False

        # only update the basics if this is the primary/canonical row
        # (indicated by the presence of a value in the item_count column)
        if item_count != '': 

            if existing:
                record.title = title
                record.commission = commission
                record.admin_notes = admin_notes
                record.total_value = total_value
                record.client = client
                record.created_at = timestamp
                record.save()

            else:
                record = Consignment(
                    legacy_id = legacy_id,
                    title = title,
                    commission = commission,
                    admin_notes = admin_notes,
                    client = client,
                    total_value = total_value
                )
                record.save()
                record.created_at = timestamp
                record.save()

        # otherwise just create a bare record to attach Artworks to
        else:
            if not existing:
                record = Consignment(
                    legacy_id = legacy_id
                )
                record.save()

        # now start dealing with line items

        # see if this artwork is already attached to the consignment
        try: attachment = ConsignmentWork.objects.get( consignment=consignment, artwork=artwork_id )
        except: attachment = None

        # bail if we already have this one
        if attachment: 
            return None

        # make sure this artwork actually exists (bail if not)
        try: 
            artwork = Artwork.objects.get( fm_id=artwork_id )
        except: 
            print("Artwork with FileMaker id " + artwork_id + " does not exist")
            return None       

        # figure out what order number to give this new entry
        order = 1
        line_items = ConsignmentWork.objects.filter( consignment=record ).order_by('-order')
        if line_items:
            order = int(line_items[0].order) + 1

        # figure out the line item value
        if line_item_value == '': line_item_value = 0

        line_item = ConsignmentWork(
            consignment = record,
            artwork = artwork,
            order = order,
            line_item_value = line_item_value
        )

        line_item.save()
        record.save()

        return record

    # ============================
    # INVOICES
    # ============================

    def get_spreadsheet_keys_for_invoice_import():
        return {
            'fm_id': {
                'column_index': 0,
                'column_name': 'Invoice Number'
            },
            'client_id': {
                'column_index': 11,
                'column_name': 'contact_id'
            },
            'artwork_id': {
                'column_index': 30,
                'column_name': 'SE_InvoiceItems::Inventory 1'
            },
            'date': {
                'column_index': 5,
                'column_name': 'Date'
            },
            'total': {
                'column_index': 2,
                'column_name': 'Amt due'
            },
            'notes': {
                'column_index': 12,
                'column_name': 'Notes'
            },
            'notes_2': {
                'column_index': 13,
                'column_name': 'Notes 2'
            },
            'row_type': {
                'column_index': 1,
                'column_name': 'line_item_num'
            }
        }

    def add_update_invoices( data = None, test = False ):

        if not test:
            ImportLog.record('import_invoices')

        if not data:
            data = Importer.import_filemaker_file( 'master-invoices', listify = True )
            header_row = data.pop(0)

        records_imported = 0
        for row in data:
            data = Importer.add_update_invoice(row)
            if data:
                records_imported = records_imported + 1
        return records_imported

    def add_update_invoice( row ):

        # get the doc keys for this import
        keys = Importer.get_spreadsheet_keys_for_invoice_import()

        # get our import values
        fm_id = row[keys['fm_id']['column_index']].strip()
        client_id = row[keys['client_id']['column_index']].strip()
        artwork_id = row[keys['artwork_id']['column_index']].strip()
        date = row[keys['date']['column_index']].strip()
        total = row[keys['total']['column_index']].strip()
        notes = row[keys['notes']['column_index']].strip()
        notes_2 = row[keys['notes_2']['column_index']].strip()
        row_type = row[keys['row_type']['column_index']].strip()

        # mark the total as zero if it's blank
        if total == '':
            total = 0

        # format the date as a Django timestamp
        if date != '':
            timestamp = Importer.clean_up_spreadsheet_timestamp( date, "%m/%d/%Y" )
        else:
            timestamp = make_aware(datetime.datetime.now())

        # glue the two different notes fields together
        admin_notes = ''
        if notes != '': admin_notes = admin_notes + notes
        if notes_2 != '': admin_notes = admin_notes + '. ' + notes_2
        admin_notes = admin_notes.strip()

        # see if we already have a match in the database
        try: 
            existing = Invoice.objects.get( fm_id = fm_id )
            record = existing
        except: 
            existing = None

        # look for an existing Contact record
        client = None
        if client_id != '':
            try: client = Contact.objects.get( legacy_id=client_id )
            except: client = None

        # only save the primary stuff if this is a primary row
        if row_type == '1':

            if existing: 

                record.fm_id = fm_id
                record.total = total
                record.admin_notes = admin_notes
                record.client = client
                record.save()

            else:

                record = Invoice(
                    fm_id = fm_id,
                    total = total,
                    admin_notes = admin_notes,
                    client = client
                )
                record.save()
                record.created_at = timestamp
                record.save()

        # else this is just a line item, so only create the barest record
        else:

            if not existing:

                record = Invoice(
                    fm_id = fm_id
                )
                record.save()
                record.created_at = timestamp
                record.save()

        # just return the record if we don't have an artwork to note
        if artwork_id == '':
            return record

        # now let's prepare to make a note about the artwork for this invoice

        # try to get a corresponding artwork
        try: artwork = Artwork.objects.get( fm_id = artwork_id )
        except: artwork = None

        this_note = "\nLine item: " + artwork_id
        if artwork:
            # change_url = reverse( 'admin:docent_artwork_change', args=(artwork.id,) )
            this_note = this_note + ": " + artwork.title
            if artwork.artist:
                this_note = this_note + " (" + artwork.artist.title + ")"
        
        new_notes = record.admin_notes + this_note
        new_notes = new_notes.strip()
        record.admin_notes = new_notes
        record.save()

        return record

    # ============================
    # TEMPORARY STUFF
    # ============================

    def merge_inventory_contacts():

        contact_file_data = Importer.import_filemaker_file('contacts-for-dict-search')
        dict_data = {}
        for row in contact_file_data:
            arr = row.rstrip().split('|')
            key = arr[2] + ", " + arr[1]
            if key not in dict_data:
                dict_data[key] = arr[0]
        
        inventory_file_data = Importer.import_filemaker_file('master-inventory-for-contact-search')
        new_inventory_file_data = []
        for row in inventory_file_data:
            orig_arr = row.rstrip().split('|')
            contact_id = ''
            if len(orig_arr) >= 2:
                key = orig_arr[1].strip('"')
                if key in dict_data:
                    contact_id = dict_data[key]
            # print( str(orig_arr[0]) + ": " + contact_id ) 
            new_arr = [ orig_arr[0], orig_arr[1], contact_id ]
            new_inventory_file_data.append(new_arr)

        path = settings.APPS_DIR
        file = "vandelay/import/master-inventory-with-contact-ids.csv"

        with open( os.path.join(path,file), 'w' ) as csvfile:
            
            # creating a csv dict writer object 
            writer = csv.writer(
                csvfile,
                delimiter = '|'
            ) 

            # writing data rows 
            writer.writerows(new_inventory_file_data) 

        return csvfile

    def figure_out_contact_artist_preferences():

        inventory_file_data = Importer.import_filemaker_file('master-contacts')
        updated_data = []
        not_found = []
        for row in inventory_file_data:

            # clean data and bust it into a list
            orig_arr = Importer.clean_quoted_csv_row(row.rstrip().split('|'))
            
            # bail if no artist preference to create
            if orig_arr[18] == '':
                # print("Bailing on row " + str(orig_arr[0]))
                updated_data.append(orig_arr)
                continue

            # bail if artist preference is already created
            if orig_arr[24] != '':
                # print("Bailing on row " + str(orig_arr[0]))
                updated_data.append(orig_arr)
                continue

            # print(orig_arr[0])
            artists = orig_arr[18].split('/')
            # print(artists)

            found_artists_string = ''
            found_artists_list = []
            if artists:
                for a in artists:
                    try: existing = Artist.objects.get( title=a )
                    except: existing = False
                    if existing: found_artists_list.append(str(existing.legacy_id))
                    else: 
                        found_artists_list.append('tktk')
                        not_found.append(a)                        
            if found_artists_list:
                found_artists_string = '/'.join(found_artists_list)
            orig_arr[24] = found_artists_string
            updated_data.append(orig_arr)
            continue

        for row in updated_data:
            print( str(row[0]) + "|" + row[24] )

    def filter_phone_numbers( data, index = None ):

        results = []

        for row in data:
            letters = []
            numbers = []
            if index: exploded = list(row[index])
            else: exploded = list(row)
            for char in exploded:
                if char.isalpha() or char == ' ': letters.append(char)
                elif char.isdigit(): numbers.append(char)
            result = {}
            result['id'] = row[0]
            result['note'] = ''.join(letters).strip().capitalize()
            result['number'] = Importer.format_phone_number(''.join(numbers).strip())
            results.append(result)

        return results

    def format_phone_number( string_num ):

        if len(string_num) == 10:
            return string_num[0:3] + '-' + string_num[3:6] + '-' + string_num[6:10]
        elif( len(string_num) == 7 ):
            return string_num[0:3] + '-' + string_num[3:7]
        else: 
            return string_num

class ImportLog(models.Model):

    process_name = models.CharField(max_length=200)
    last_run = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.process_name

    def record(process_name):
        log = ImportLog(process_name=process_name)
        log.save()

class Exporter(models.Model):

    class Meta:
        abstract = True

    def export_image_ar():

        # set up the file
        now = datetime.datetime.now()
        datestamp = now.strftime( "%Y%m%d%H%M" )
        path = settings.APPS_DIR
        file = "vandelay/export/ar-reports/" + datestamp + ".csv"

        # get the relevant Image records
        query_results = Image.objects.exclude( ar_ios__isnull = True )

        # bail if we didn't get anything
        if not query_results:
            return

        # set up the header row
        fields = [
            'id',
            'ios',
            'android'
        ]

        # get just the fields we want from the query results
        file_results = []
        for result in query_results:
            if not result.legacy_id: continue
            this = {}
            this['id'] = result.legacy_id
            this['ios'] = result.ar_ios
            this['android'] = result.ar_android
            file_results.append(this)

        # set up a response object, which will force a download dialogue
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="' + datestamp + '.csv"'

        # pass this response to the Writer object
        writer = csv.DictWriter(
            response, 
            delimiter = '|',
            fieldnames = fields
        )

        # write the header row
        writer.writeheader() 

        # writing the rest of the rows
        writer.writerows(file_results) 

        return response