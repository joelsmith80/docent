var WwScourRangeControls = (function($){

  // nod to: https://goodies.pixabay.com/jquery/auto-complete/demo.html  

  $ = jQuery.noConflict();

  'use strict';

  // placeholder for cached DOM elements
  var DOM = {};

  /* ===== private stuff ===== */

  function cacheDom(){
    DOM.$multiRanges = $('.multirange-input');
    DOM.$dummies = $('.multirange-dummy');
    DOM.$nonJSControls = $('.search-filters .hide-if-js');
  }

  function bindEvents(){

    DOM.$nonJSControls.hide();
    
    DOM.$multiRanges.each(function(index){
      var step = $(this).data('step');
      var min_limit = $(this).data('min');
      var max_limit = $(this).data('max');
      var min_value = $(this).data('min-value');
      var max_value = $(this).data('max-value');
      var min_target_raw = $(this).attr('data-min-target');
      var min_target = $("#" + min_target_raw);
      var max_target_raw = $(this).attr('data-max-target');
      var max_target = $("#" + max_target_raw);
      var units = $(this).data('units');
      $(this).slider({
        range: true,
        min: min_limit,
        max: max_limit, 
        values: [ min_value, max_value ],
        step: step,
        slide: function( event, ui ) {
          updateTargets( min_target, max_target, ui.values[0], ui.values[1] );
          if(units == 'dollars'){
            $( this ).prev().val( "$" + formatInteger(ui.values[ 0 ]) + " - $" + formatInteger(ui.values[ 1 ]) );
          } else {
            $( this ).prev().val( formatInteger(ui.values[ 0 ]) + '" - ' + formatInteger(ui.values[ 1 ]) + '"' );
          }
        }
      });
    });
    
    DOM.$dummies.each(function(index){
      var min = $(this).next().slider('values',0);
      var max = $(this).next().slider('values',1);
      var units = ($(this).attr('data-units') === 'dollars') ? '$' : '"';
      if($(this).attr('data-units') === 'dollars'){
        $(this).val('$' + formatInteger(min) + ' - $' + formatInteger(max));
      } else {
        $(this).val(formatInteger(min) + '" - ' + formatInteger(max) + '"');
      }
    });
  }

  function updateTargets( min_target, max_target, min_val, max_val ){
    $(min_target).val(min_val);
    $(max_target).val(max_val);
  }

  function formatInteger( int ){
    return int.toLocaleString('en');
  }

  /* ===== public stuff ====== */

  // main init method
  function init(){
    cacheDom();
    bindEvents();
  }

  /* ===== export public methods ===== */

  return {
      init: init
  };   

}());

jQuery(document).ready(function($){

  WwScourRangeControls.init();

});