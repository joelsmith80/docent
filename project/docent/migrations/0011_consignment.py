# Generated by Django 2.2.5 on 2020-05-12 21:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('docent', '0010_auto_20200512_1319'),
    ]

    operations = [
        migrations.CreateModel(
            name='Consignment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('legacy_id', models.BigIntegerField(blank=True, null=True, unique=True)),
                ('title', models.CharField(max_length=200)),
                ('commission', models.CharField(blank=True, default='', max_length=200)),
                ('admin_notes', models.TextField(blank=True, default='')),
                ('terms', models.TextField(blank=True, default='')),
                ('accounting_notes', models.TextField(blank=True, default='')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
