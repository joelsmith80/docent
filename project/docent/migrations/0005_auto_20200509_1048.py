# Generated by Django 2.2.5 on 2020-05-09 17:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('docent', '0004_auto_20200508_1410'),
    ]

    operations = [
        migrations.AddField(
            model_name='artworkvariation',
            name='legacy_id',
            field=models.BigIntegerField(blank=True, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='artworkvariation',
            name='artwork',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='variations', to='docent.Artwork'),
        ),
    ]
