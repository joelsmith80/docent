# Generated by Django 2.2.5 on 2020-05-16 00:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('docent', '0016_auto_20200515_0905'),
    ]

    operations = [
        migrations.AddField(
            model_name='consignmentwork',
            name='line_item_value',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
        ),
    ]
