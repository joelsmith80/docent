# Generated by Django 2.2.5 on 2020-05-14 17:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('docent', '0012_auto_20200513_0915'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='democollection',
            options={'verbose_name': 'Demo Collection', 'verbose_name_plural': 'Demo Collections'},
        ),
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('fm_id', models.CharField(blank=True, default='', max_length=50, verbose_name='FileMaker Id')),
                ('snipcart_id', models.CharField(blank=True, default='', max_length=50, verbose_name='Snipcart Id')),
                ('total', models.DecimalField(decimal_places=2, default=0, max_digits=10)),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='docent.Contact')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
