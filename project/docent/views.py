from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator
from datetime import datetime
from django.db.models import Count

from .models import Exhibition, Artist, Post, Artwork, SiteOption
from .forms import SearchCollectionForm
from project.utils.helpers import *


def home(request):
    return render(request, "home.html")


def exhibition_index(request, page=None):
    now = datetime.now()
    page = int(page) if page else 1
    posts_per_page = Exhibition.get_posts_per_page()
    upcoming_exhibits_classes = "full"
    title_section = "past"
    exhibitions_current = None
    exhibitions_upcoming = None
    exhibitions_past = Exhibition.get_past()
    paginator = Paginator(exhibitions_past, posts_per_page)
    exhibitions_past = paginator.get_page(page)

    # if this is the first page, we need Upcoming and Current (otherwise just Past)
    if page and page == 1:

        exhibitions_current = Exhibition.get_current()

        # sort stuff out for the Upcoming exhibitions
        exhibitions_upcoming = Exhibition.get_upcoming()
        if exhibitions_upcoming.exists():
            result_count = exhibitions_upcoming.count()
            if result_count == 2:
                upcoming_exhibits_classes = "m1-one m2-two t-two d-two w-two"
            elif result_count > 2:
                upcoming_exhibits_classes = "m1-one m2-two t-three d-three w-three"

    # figure out which section title should be the page title
    if not exhibitions_current:
        title_section = "upcoming"
        if not exhibitions_upcoming:
            title_section = "past"

    context = {
        "exhibitions_current": exhibitions_current,
        "exhibitions_past": exhibitions_past,
        "exhibitions_upcoming": exhibitions_upcoming,
        "upcoming_exhibits_classes": upcoming_exhibits_classes,
        "title_section": title_section,
    }
    return render(request, "exhibition/index.html", context)


def exhibition_detail(request, slug):
    exhibition = get_object_or_404(Exhibition, slug=slug)
    context = {"exhibition": exhibition}
    return render(request, "exhibition/detail.html", context)


def artist_index(request):
    artists_active = Artist.objects.filter(active_status="active",available=True).order_by("last_name")
    artists_inactive = Artist.objects.filter(active_status="inactive",available=True).order_by("last_name")
    context = {
        "artists_active": artists_active,
        "artists_inactive": artists_inactive,
        "main_classes": "pg-artists content",
    }
    return render(request, "artist/index.html", context)


def artist_detail(request, slug):
    artist = get_object_or_404(Artist, slug=slug)
    context = {
        "artist": artist,
        "main_classes": "content single-artist",
    }
    return render(request, "artist/detail.html", context)


def news_index(request):
    posts = Post.objects.filter(post_status="published").order_by("created_at")
    context = {"posts": posts, "main_classes": "news-index"}
    return render(request, "news/index.html", context)


def news_detail(request, slug):
    post = get_object_or_404(Post, slug=slug)
    context = {
        "post": post,
        "main_classes": "content single-post",
    }
    return render(request, "news/detail.html", context)


def search_collection(request, *args, **kwargs):

    # separate search manager:
    # https://wellfire.co/learn/simple-search-manager-methods/

    form = SearchCollectionForm(request.GET)
    if form.is_valid():
        resultz = Artwork.search_objects.search( **form.cleaned_data )

    results = Artwork.objects.filter(post_status='published', searchable=True)

    if 'price_min' in request.GET:
        price_min = int(request.GET['price_min'])
        results = results.filter(variations__total_price__gte=price_min)

    if 'price_max' in request.GET:
        price_max = int(request.GET['price_max'])
        results = results.filter(variations__total_price__lte=price_max)

    if 'height_min' in request.GET:
        height_min = int(request.GET['height_min'])
        results = results.filter(variations__height__gte=height_min)

    if 'height_max' in request.GET:
        height_max = int(request.GET['height_max'])
        results = results.filter(variations__height__lte=height_max)

    if 'width_min' in request.GET:
        width_min = int(request.GET['width_min'])
        results = results.filter(variations__width__gte=width_min)

    if 'width_max' in request.GET:
        width_max = int(request.GET['width_max'])
        results = results.filter(variations__width__lte=width_max)

    if 'mediums' in request.GET:
        sent = request.GET.get('mediums')
        if sent.strip() != '':
            received = filter_url_variables(sent)
            if received:
                num_received = len(received)
                results = results.filter( work_mediums__in=received ).annotate( num_mediums=Count('work_mediums') ).filter(num_mediums=num_received)

    if 'aspects' in request.GET:
        sent = request.GET.get('aspects')
        if sent.strip() != '':
            received = filter_url_variables(sent)
            if received:
                num_received = len(received)
                results = results.filter( work_aspects__in=received ).annotate( num_aspects=Count('work_aspects') ).filter(num_aspects=num_received)

    if 'artists' in request.GET:
        sent = request.GET.get('artists')
        if sent.strip() != '':
            received = filter_url_variables(sent)
            if received:
                results = results.filter( artist__id__in=received )

    results = results.distinct()[:10]

    filters = Artwork.get_search_filters(request.GET)
    filters_chosen = []
    for key, entry in filters.items():
        if entry['selected']:
            filters_chosen.append((key,entry))

    context = {
        'form': form,
        'results': results, 
        'num_results': len(results),
        'resultz': resultz,
        'filters': filters,
        'filters_chosen': filters_chosen,
        'main_classes': 'pg-search search-index content'
    }

    """
    get $for variables:
    - number of works
    - x price range low
    - x price range high
    - x height range low
    - x height range high
    - x width range low
    - x width range high
    - x mediums (selected or not)
    - x visual aspects (selected or not)
    - x artists (selected or not)
    """
    return render(request, "artworks/index.html", context)


def artwork_detail(request, slug):

    try:
        artwork = Artwork.objects.get(slug=slug, searchable=True)
    except:
        return redirect("docent:search_collection")

    details_list = artwork.get_details_list()

    other_works = None
    if artwork.artist:
        other_works = artwork.artist.get_works(artwork.id)
    print(other_works)
    # other_works = artwork.get_works( exclude_ids = [] )

    context = {}
    context["artwork"] = artwork
    context["details_list"] = details_list
    context["other_works"] = other_works
    return render(request, "artworks/detail.html", context)

