from django.apps import AppConfig


class DocentConfig(AppConfig):
    name = 'docent'
