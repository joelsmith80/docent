from django.test import TestCase
from . models import Image

from project.vandelay.models import Importer

# make sure Image records that don't call for AR don't query the api
class MockHoudiniNoAR(TestCase):

    def setUp(self):

        # create a test image
        image = Image(
            title = 'Test Image',
            slug = 'test-image',
            should_fetch_ar = False
        )
        image.save
        self.image = image

    def test_query_fire(self):
        data = self.image.maybe_fetch_AR()
        self.assertFalse( data )

# make sure Image records that don't have an image don't query the api
class MockHoudiniNoImage(TestCase):

    def setUp(self):

        # create a test image
        image = Image(
            title = 'Test Image',
            slug = 'test-image',
            should_fetch_ar = True
        )
        image.save
        self.image = image

    def test_query_fire(self):
        data = self.image.maybe_fetch_AR()
        self.assertFalse( data )

        # MockHoudiniNoAR
        # MockHoudiniNoImage
        # MockHoudiniBrokenImage
        # MockHoudiniNeedsAR

        # test that
            # an image that shouldn't fetch ar doesn't fetch ar
            # an Image record that doesn't have an image doesn't call the api
            # an Image that doesn't have a VALID image doesn't call the api
            # an image that needs and doesn't have ar
                # gets a response
                # turns off should_fetch_ar
                # gets both ios and ar model urls
                    # properly saves ios and ar model urls
                    # (writes a Milestone)

class TestHoudiniAPI(TestCase):

    def setUp(self):
    
        # create a test record
        self.imageRecord = Image(
            title = "Quantam Foam",
            slug = "quantam-foam",
            legacy_id = 6350,
            ww_img = "https://seattle.winstonwachter.com/wp-content/uploads/2020/02/Brown_QuantumFoam_2019_CutPaper_34x34inches_web.jpg"
        )
        self.imageRecord.save()

        # give the test record an actual image (using the wp image)
        self.imageRecord = Importer.add_update_image_file_from_own_url(self.imageRecord.legacy_id)

        # fetch Houdini API for image
        self.data = self.imageRecord.fetch_AR()

        # turn on test record's Fetch option to trigger fetch on save
        self.imageRecord.should_fetch_ar = True
        self.imageRecord.save()

    def tearDown(self):
        self.imageRecord.img.delete_sized_images()

    def test_record_was_created(self):
        self.assertTrue( self.imageRecord )

    def test_record_has_img(self):
        self.assertTrue( self.imageRecord.img )

    def test_api_got_data_response(self):
        self.assertTrue(self.data)

    def test_api_produced_ar_for_ios(self):
        self.assertTrue(self.data['iosURL'])

    def test_api_produced_ar_for_android(self):
        self.assertTrue(self.data['androidURL'])

class TestHoudiniFetchFlow(TestCase):

    def setUp(self):
    
        # create a test record
        self.imageRecord = Image(
            title = "Quantam Foam",
            slug = "quantam-foam",
            legacy_id = 6350,
            ww_img = "https://seattle.winstonwachter.com/wp-content/uploads/2020/02/Brown_QuantumFoam_2019_CutPaper_34x34inches_web.jpg"        )
        self.imageRecord.save()

        # give the test record an actual image (using the wp image)
        self.imageRecord = Importer.add_update_image_file_from_own_url(self.imageRecord.legacy_id)

        # turn on test record's Fetch option to trigger fetch on save
        self.imageRecord.should_fetch_ar = True
        self.imageRecord.save()

    def tearDown(self):
        self.imageRecord.img.delete_sized_images()

    def test_image_record_now_has_ar_ios(self):
        self.assertTrue(self.imageRecord.ar_ios)

    def test_image_record_now_has_ar_android(self):
        self.assertTrue(self.imageRecord.ar_android)

    