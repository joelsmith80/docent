from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import Video, Post, Artist, Exhibition, Artwork, ArtworkVariation, ArtworkMedium, ArtworkAspect, ArtworkType, ArtworkLocation, ArtworkSignatureStatus, Image, Contact, ContactCategory, Address, Telephone, Email, EmailList, SiteOption, DemoCollection, Consignment, ConsignmentStatus, Moment, Invoice, Document
from project.vandelay.models import Importer

# model: Image
def create_crops( modeladmin, request, queryset ):
    for obj in queryset:
        Importer.add_update_image_file_from_own_url(obj.legacy_id)

# model: Artwork
def populate_featured_image( modeladmin, request, queryset ):
    for obj in queryset:
        Importer.make_artwork_featured_image_from_work_images(obj)

class ArtworkImageInlineAdmin(admin.TabularInline):
    model = Artwork.work_images.through
    extra = 0

class ArtworkHistoryInlineAdmin(admin.TabularInline):
    model = Moment
    extra = 0
    fields = ('description','created_at','updated_at')
    readonly_fields = ('description','created_at','updated_at')

class ExhibitionImageInlineAdmin(admin.TabularInline):
    model = Exhibition.images.through
    extra = 0

class ArtistImageInlineAdmin(admin.TabularInline):
    model = Artist.work_images.through
    extra = 0

class ArtistInstallImageInlineAdmin(admin.TabularInline):
    model = Artist.install_images.through
    extra = 0

class DemoCollectionWorkInlineAdmin(admin.TabularInline):
    model = DemoCollection.artworks.through
    extra = 0

class ConsignmentWorksInlineAdmin(admin.TabularInline):
    model = Consignment.artworks.through
    extra = 0

class VideoAdmin(admin.ModelAdmin):
    list_display = ('id','title','legacy_id','slug','created_at','updated_at',)
    list_display_links = ('id','title',)
    ordering = ['-created_at']

class PostAdmin(admin.ModelAdmin):
    list_display = ('id','title','slug','created_at','updated_at',)
    list_display_links = ('id','title',)
    ordering = ['-created_at']

class ExhibitionAdmin(admin.ModelAdmin):
    list_display = ('id','title','legacy_id','slug','post_status','created_at','updated_at',)
    list_display_links = ('id','title',)
    search_fields = ('title',)
    ordering = ['-created_at']
    inlines = [ExhibitionImageInlineAdmin,]

class ArtistAdmin(admin.ModelAdmin):
    list_display = ('id','title','legacy_id','slug','created_at','updated_at',)
    list_display_links = ('id','title',)
    search_fields = ('title',)
    ordering = ['last_name']
    inlines = [ArtistImageInlineAdmin,ArtistInstallImageInlineAdmin,]

class ArtworkVariationInline(admin.StackedInline):
    model = ArtworkVariation
    extra = 0

class ArtworkAdmin(admin.ModelAdmin):
    list_display = ('id','featured_image_thumbnail','title','artist','legacy_id','created_at','updated_at',)
    list_display_links = ('id','title','featured_image_thumbnail',)
    # fields = ('title','slug','legacy_id','post_status','description','work_mediums','work_aspects','work_types','featured_image',)
    fieldsets = (
        (None, {
            'fields': ('title','slug','artist','year','description','searchable',)
        }),
        (_('Images'), {
            'fields': ('featured_image',),
        }),
        (_('Taxonomy'), {
            'fields': ('work_mediums','work_aspects','work_types',),
            'classes': ('collapse',),
        }),
        (_('Misc'), {
            'fields': ('legacy_id','fm_id',)
        }),
    )
    search_fields = ('title','artist__title',)
    actions = [
        populate_featured_image
    ]
    inlines = [ArtworkImageInlineAdmin,ArtworkVariationInline,ArtworkHistoryInlineAdmin,]

class ArtworkMediumAdmin(admin.ModelAdmin):
    list_display = ('id','title','slug',)
    list_display_links = ('id','title',)

class ArtworkAspectAdmin(admin.ModelAdmin):
    list_display = ('id','title','slug',)
    list_display_links = ('id','title',)
    exclude = ('legacy_id',)

class ArtworkTypeAdmin(admin.ModelAdmin):
    list_display = ('id','title','slug','legacy_id',)
    list_display_links = ('id','title',)
    # exclude = ('legacy_id',)

class ArtworkLocationAdmin(admin.ModelAdmin):
    list_display = ('id','title','slug','legacy_id',)
    list_display_links = ('id','title',)
    # exclude = ('legacy_id',)

class ArtworkSignatureStatusAdmin(admin.ModelAdmin):
    list_display = ('id','title','slug',)
    list_display_links = ('id','title',)
    exclude = ('legacy_id',)

class ImageAdmin(admin.ModelAdmin):
    list_display = ('id','table_thumbnail','title','legacy_id','created_at','updated_at',)
    list_display_links = ('id','table_thumbnail','title',)
    search_fields = ('title',)
    actions = [
        create_crops
    ]

class ContactAddressInline(admin.TabularInline):
    model = Address
    extra = 0

class ContactTelephoneInline(admin.TabularInline):
    model = Telephone
    extra = 0

class ContactEmailInline(admin.TabularInline):
    model = Email
    extra = 0

class ContactAdmin(admin.ModelAdmin):
    list_display = ('id','last_name','first_name','record_name','legacy_id','company','created_at','updated_at',)
    list_display_links = ('id','record_name',)
    search_fields = ('last_name','first_name','company',)
    autocomplete_fields = ['artists','categories',]
    inlines = [ ContactAddressInline, ContactTelephoneInline, ContactEmailInline ]

class MailRecipientsInline(admin.TabularInline):
    model = Email.lists.through

class EmailListAdmin(admin.ModelAdmin):
    list_display = ('id','title','created_at','updated_at',)
    list_display_links = ('id','title',)
    inlines = [MailRecipientsInline]

class ContactCategoryAdmin(admin.ModelAdmin):
    list_display = ('id','title','slug',)
    list_display_links = ('id','title',)
    exclude = ('legacy_id',)
    search_fields = ('title',)

class OptionAdmin(admin.ModelAdmin):
    list_display = ('id','opt_key','opt_value')
    list_display_links = ('opt_key',)
    ordering = ['opt_key']

class DemoCollectionAdmin(admin.ModelAdmin):
    list_display = ('id','title','created_at','updated_at',)
    list_display_links = ('id','title',)
    inlines = [DemoCollectionWorkInlineAdmin,]

class ConsignmentAdmin(admin.ModelAdmin):
    list_display = ('id','title','created_at','updated_at',)
    list_display_links = ('id','title',)
    inlines = [ConsignmentWorksInlineAdmin,]

class ConsignmentStatusAdmin(admin.ModelAdmin):
    list_display = ('id','title','slug',)
    list_display_links = ('id','title',)
    exclude = ('legacy_id',)
    search_fields = ('title',)

class MomentAdmin(admin.ModelAdmin):
    list_display = ('id','created_at','updated_at')

class InvoiceAdmin(admin.ModelAdmin):
    list_display = ('id','fm_id','snipcart_id','total','created_at','updated_at',)

class DocumentAdmin(admin.ModelAdmin):
    list_display = ('id','title','created_at','updated_at',)
    fields = ('title','slug','description','document_file',)

admin.site.register(Video,VideoAdmin)
admin.site.register(Post,PostAdmin)
admin.site.register(Exhibition,ExhibitionAdmin)
admin.site.register(Artist,ArtistAdmin)
admin.site.register(Artwork,ArtworkAdmin)
# admin.site.register(ArtworkType, ArtworkTypeAdmin)
admin.site.register(ArtworkMedium, ArtworkMediumAdmin)
admin.site.register(ArtworkAspect, ArtworkAspectAdmin)
admin.site.register(ArtworkType, ArtworkTypeAdmin)
admin.site.register(ArtworkLocation, ArtworkLocationAdmin)
admin.site.register(ArtworkSignatureStatus, ArtworkSignatureStatusAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(ContactCategory, ContactCategoryAdmin)
admin.site.register(EmailList,EmailListAdmin)
admin.site.register(SiteOption, OptionAdmin)
admin.site.register(DemoCollection, DemoCollectionAdmin)
admin.site.register(Consignment, ConsignmentAdmin)
admin.site.register(ConsignmentStatus, ConsignmentStatusAdmin)
admin.site.register(Moment,MomentAdmin)
admin.site.register(Invoice,InvoiceAdmin)
admin.site.register(Document,DocumentAdmin)