from django.utils.six import BytesIO
from PIL import Image
from versatileimagefield.datastructures import SizedImage
from versatileimagefield.registry import versatileimagefield_registry

# this checks if an image is over the width set in settings.VERSATILEIMAGEFIELD_RENDITION_KEY_SETS
# if it is, it sets the new width at the width limit,
# calculates the original aspect ratio of the image, 
# and sets a new proportional height
class MaxNatural(SizedImage):

    filename_key = 'max-natural'

    def process_image(self, image, image_format, save_kwargs,
                      width, height):
        """
        Returns a BytesIO instance of `image` that will fit
        within a bounding box as specified by `width`x`height`
        """

        if image.width > width:
            aspect_ratio = image.width / image.height
            new_height = width / aspect_ratio
            new_height = round(new_height)
            height = new_height

        imagefile = BytesIO()
        image.thumbnail(
            (width, height),
            Image.ANTIALIAS
        )
        image.save(
            imagefile,
            **save_kwargs
        )
        return imagefile

# Registering the ThumbnailSizer to be available on VersatileImageField
# via the `thumbnail` attribute
versatileimagefield_registry.register_sizer('max-natural', MaxNatural)