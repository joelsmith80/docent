# Generated by Django 2.2.5 on 2020-03-29 17:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('docent', '0024_auto_20200323_0012'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='ww_img',
            field=models.URLField(blank=True, null=True),
        ),
    ]
