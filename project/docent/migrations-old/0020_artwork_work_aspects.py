# Generated by Django 2.2.5 on 2020-03-15 00:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('docent', '0019_artwork_work_mediums'),
    ]

    operations = [
        migrations.AddField(
            model_name='artwork',
            name='work_aspects',
            field=models.ManyToManyField(blank=True, related_name='aspects', to='docent.ArtworkAspect', verbose_name='Work Aspect(s)'),
        ),
    ]
