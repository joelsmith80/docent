# Generated by Django 2.2.5 on 2020-04-09 18:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('docent', '0032_auto_20200409_1822'),
    ]

    operations = [
        migrations.AddField(
            model_name='artwork',
            name='featured_image',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='featured_image', to='docent.Image'),
        ),
    ]
