# Generated by Django 2.2.5 on 2020-03-13 00:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('docent', '0010_artworkmedium'),
    ]

    operations = [
        migrations.AddField(
            model_name='artworkmedium',
            name='legacy_id',
            field=models.PositiveIntegerField(blank=True, null=True, unique=True),
        ),
    ]
