# Generated by Django 2.2.5 on 2020-04-30 15:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('docent', '0041_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='address',
            name='primary',
            field=models.BooleanField(default=0, verbose_name='Primary address?'),
        ),
    ]
